package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.Product;
import com.qqs.jwlsvcs.service.ProductService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/product")
public class ProductController {
    @Resource
    ProductService productService;
    

    // Product

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = "application/json")
    public ResponseEntity<Product> saveProduct(@RequestBody Product form) throws QQBusinessException {
        Product saved = productService.saveProduct(form);
        ResponseEntity<Product> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_OPERATOR', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.POST, value = "/saveProdTrans", produces = "application/json")
    public ResponseEntity<Product> saveProductTrans(@RequestBody Product form) throws QQBusinessException {
        Product saved = productService.saveProductTrans(form);
        ResponseEntity<Product> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR' )")
    @RequestMapping(method = RequestMethod.GET, value = "/product/byId", produces = "application/json")
    public ResponseEntity<Collection<Product>> getProductById(HttpServletRequest request,
                                                                @RequestParam Integer id) throws QQBusinessException {
        Product product = productService.getProductById(id);
        ResponseEntity<Collection<Product>> result = new ResponseEntity(product, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/product/byBarCode", produces = "application/json")
    public ResponseEntity<Collection<Product>> getProductByBarCode(HttpServletRequest request,
                                                              @RequestParam String barCode, Boolean latestTran) throws QQBusinessException {
        Product product = productService.getProductByBarCode(barCode, latestTran);
        ResponseEntity<Collection<Product>> result = new ResponseEntity(product, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/searchProduct", produces = "application/json")
    public ResponseEntity<List<Product>> searchProduct(@RequestParam Map<String, String> searchParam,
                                                           @RequestParam(required = false) Optional<Boolean> exactMatch,
                                                           HttpServletRequest request) throws QQBusinessException {
        List<Product> formList = productService.searchProduct(searchParam, exactMatch.orElse(false));
        ResponseEntity<List<Product>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/searchBarcode", produces = "application/json")
    public ResponseEntity<Integer> searchProductByProductMasterId(@RequestParam Integer id,
                                                       HttpServletRequest request) throws QQBusinessException {

        Integer maxSeries = productService.searchBarcodeByProductMasterId(id);
        ResponseEntity<Integer> result = new ResponseEntity(maxSeries, HttpStatus.OK);

        return result;
    }
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/searchProductbymasterId", produces = "application/json")
    public ResponseEntity<List<Product>> searchProductsByProductMasterId(@RequestParam Integer id,
                                                       HttpServletRequest request) throws QQBusinessException {

        List<Product> productList = productService.searchProductsByProductMasterId(id);
        ResponseEntity<List<Product>> result = new ResponseEntity(productList, HttpStatus.OK);

        return result;
    }



}
