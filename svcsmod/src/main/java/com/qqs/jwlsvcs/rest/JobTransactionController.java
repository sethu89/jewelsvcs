package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.JobTransaction;
import com.qqs.jwlsvcs.api.JobTransactionData;
import com.qqs.jwlsvcs.service.JobTransactionService;
import com.qqs.jwlsvcs.service.ReportService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/jobtrans")
public class JobTransactionController {
    @Resource
    JobTransactionService jobTransactionService;

    @Resource
    private ReportService service;

    // JobTransaction

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = "application/json")
    public ResponseEntity<JobTransaction> saveJobTransaction(@RequestBody JobTransaction form) throws QQBusinessException {
        JobTransaction saved = jobTransactionService.saveJobTransaction(form);
        ResponseEntity<JobTransaction> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/jobTransaction/byId", produces = "application/json")
    public ResponseEntity<Collection<JobTransaction>> getJobTransactionById(HttpServletRequest request,
                                                                @RequestParam Integer id) throws QQBusinessException {
        JobTransaction jobTransaction = jobTransactionService.getJobTransactionById(id);
        ResponseEntity<Collection<JobTransaction>> result = new ResponseEntity(jobTransaction, HttpStatus.OK);
        return result;
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/searchJobTransaction", produces = "application/json")
    public ResponseEntity<List<JobTransaction>> searchJobTransaction(@RequestParam Map<String, String> searchParam,
                                                           @RequestParam(required = false) Optional<Boolean> exactMatch,
                                                           HttpServletRequest request) throws QQBusinessException {
        List<JobTransaction> formList = jobTransactionService.searchJobTransaction(searchParam, exactMatch.orElse(false));
        ResponseEntity<List<JobTransaction>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.GET, value = "/jobTransactionData", produces = "application/json")
    public ResponseEntity<JobTransactionData> getJobTransactionData(@RequestParam Map<String, String> searchParam,
                                                                    HttpServletRequest request) throws QQBusinessException {
        List<JobTransactionData> jobTransactionData = service.getJobTransactionData(searchParam);
        ResponseEntity<JobTransactionData> result = new ResponseEntity(jobTransactionData, HttpStatus.OK);
        return result;
    }



}
