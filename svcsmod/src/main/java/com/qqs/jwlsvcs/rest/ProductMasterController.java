package com.qqs.jwlsvcs.rest;
import com.qqs.jwlsvcs.api.ProductMaster;

import com.qqs.jwlsvcs.api.PropAttributes;
import com.qqs.jwlsvcs.service.ProductMasterService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/productmaster")
public class ProductMasterController {
    @Resource
    ProductMasterService productMasterService;
    

    // ProductMaster

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = "application/json")
    public ResponseEntity<ProductMaster> saveProductMaster(@RequestBody com.qqs.jwlsvcs.api.ProductMaster form) throws QQBusinessException {
        com.qqs.jwlsvcs.api.ProductMaster saved = productMasterService.saveProductMaster(form);
        ResponseEntity<ProductMaster> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/allProductMaster", produces = "application/json")
    public ResponseEntity<List<ProductMaster>> getAllProductMaster(HttpServletRequest request) throws QQBusinessException {
        List<ProductMaster> formList = productMasterService.getAllProductMaster();
        ResponseEntity<List<ProductMaster>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/productMaster/byId", produces = "application/json")
    public ResponseEntity<Collection<ProductMaster>> getProductMasterById(HttpServletRequest request,
                                                                @RequestParam Integer id) throws QQBusinessException {
        ProductMaster productMaster = productMasterService.getProductMasterById(id);
        ResponseEntity<Collection<ProductMaster>> result = new ResponseEntity(productMaster, HttpStatus.OK);
        return result;
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/searchProductMaster", produces = "application/json")
    public ResponseEntity<List<ProductMaster>> searchProductMaster(@RequestParam Map<String, String> searchParam,
                                                           @RequestParam(required = false) Optional<Boolean> exactMatch,
                                                           HttpServletRequest request) throws QQBusinessException {
        List<ProductMaster> formList = productMasterService.searchProductMaster(searchParam, exactMatch.orElse(false));
        ResponseEntity<List<ProductMaster>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/allPropAttributes/byProductMasterId", produces = "application/json")
    public ResponseEntity<List<PropAttributes>> getAllCitiesByStateId(HttpServletRequest request,
                                                                      @RequestParam Integer productMasterId) throws QQBusinessException {
        List<PropAttributes> formList = productMasterService.getPropAttributesByProductMasterId(productMasterId);
        ResponseEntity<List<PropAttributes>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }


}
