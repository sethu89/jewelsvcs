package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.Desk;
import com.qqs.jwlsvcs.service.DeskService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/desk")
public class DeskController {
    @Resource
    DeskService deskService;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = "application/json")
    public ResponseEntity<Desk> saveDesk(@RequestBody com.qqs.jwlsvcs.api.Desk form) throws QQBusinessException {
        com.qqs.jwlsvcs.api.Desk saved = deskService.saveDeskForm(form);
        ResponseEntity<Desk> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "search/byId", produces = "application/json")
    public ResponseEntity<Collection<Desk>> getDeskById(HttpServletRequest request,
                                                                          @RequestParam Integer id) throws QQBusinessException {
        Desk deskMaster = deskService.getDeskById(id);
        ResponseEntity<Collection<Desk>> result = new ResponseEntity(deskMaster, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/searchDesk", produces = "application/json")
    public ResponseEntity<List<Desk>> searchDesk(@RequestParam Map<String, String> searchParam,
                                                                   @RequestParam(required = false) Optional<Boolean> exactMatch,
                                                                   HttpServletRequest request) throws QQBusinessException {
        List<Desk> formList = deskService.searchDesk(searchParam, exactMatch.orElse(false));
        ResponseEntity<List<Desk>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }
}
