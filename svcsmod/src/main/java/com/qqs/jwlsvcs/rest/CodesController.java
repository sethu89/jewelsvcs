package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.common.CodeAssociation;
import com.qqs.jwlsvcs.api.common.Codes;
import com.qqs.jwlsvcs.service.CodesService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/codes")
public class CodesController {

    @Resource
    CodesService codesService;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = "application/json")
    public ResponseEntity<Codes> saveCodes(@RequestBody Codes form) throws QQBusinessException {
        Codes saved = codesService.saveCodes(form);
        ResponseEntity<Codes> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/search/byId", produces = "application/json")
    public ResponseEntity<Codes> getCodesById(@RequestParam Integer id,
                                                  HttpServletRequest request) throws QQBusinessException {
        Codes codes = codesService.getCodesById(id);
        ResponseEntity<Codes> result = new ResponseEntity(codes, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/search", produces = "application/json")
    public ResponseEntity<List<Codes>> searchCodes(@RequestParam Map<String, String> searchParam,
                                                      @RequestParam(required = false) Optional<Boolean> exactMatch,
                                                      HttpServletRequest request) throws QQBusinessException {
        List<Codes> formList = codesService.searchCodes(searchParam, exactMatch.orElse(false));
        ResponseEntity<List<Codes>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.POST, value = "/saveCodeAssoc", produces = "application/json")
    public ResponseEntity<CodeAssociation> saveCodeAssoc(@RequestBody CodeAssociation form) throws QQBusinessException {
        CodeAssociation saved = codesService.saveCodeAssociation(form);
        ResponseEntity<CodeAssociation> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/searchCodeAssoc/byId", produces = "application/json")
    public ResponseEntity<CodeAssociation> getCodeAssocById(@RequestParam Integer id,
                                              HttpServletRequest request) throws QQBusinessException {
        CodeAssociation codeAssociation = codesService.getCodeAssociationById(id);
        ResponseEntity<CodeAssociation> result = new ResponseEntity(codeAssociation, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/form/searchCodeAssoc", produces = "application/json")
    public ResponseEntity<List<CodeAssociation>> searchCodeAssoc(@RequestParam Map<String, String> searchParam,
                                                   @RequestParam(required = false) Optional<Boolean> exactMatch,
                                                   HttpServletRequest request) throws QQBusinessException {
        List<CodeAssociation> formList = codesService.searchCodeAssociations(searchParam, exactMatch.orElse(false));
        ResponseEntity<List<CodeAssociation>> result = new ResponseEntity(formList, HttpStatus.OK);
        return result;
    }
}
