package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.*;
import com.qqs.jwlsvcs.api.common.Codes;
import com.qqs.jwlsvcs.api.translation.ParentEntityType;
import com.qqs.jwlsvcs.service.*;
import com.qqs.jwlsvcs.service.startup.ApplicationCodeMap;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.CurrencyUtils;
import com.qqs.qqsoft.utils.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.*;

import static com.qqs.jwlsvcs.utils.Constants.*;


@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/support")
public class SupportController {

    @Resource
    private ApplicationCodeMap codeMap;

    @Resource
    private DashboardService dashboardService;

    @Resource
    private BarCodeService barCodeService;

    @Value("${app.document.upload.location}")
    private String uploadFolder;

    // PICKLIST
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/picklist", produces = "application/json")
    public ResponseEntity<Map<String, Map<String, Codes>>> getPickList(HttpServletRequest request) throws QQBusinessException {
        ResponseEntity<Map<String, Map<String, Codes>>> result = new ResponseEntity(codeMap.getPickListMap(), HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/picklist/refresh", produces = "application/json")
    public ResponseEntity<String> refreshPicklist(HttpServletRequest request) throws QQBusinessException {
        codeMap.fillCodeData();
        ResponseEntity<String> result = new ResponseEntity("Refresh Successful", HttpStatus.OK);
        return result;
    }



    // CONVERSION AMOUT
    @RequestMapping(method = RequestMethod.GET, value = "/convertword", produces = "application/json")
    public ResponseEntity<String> getAddressByParent(HttpServletRequest request,
                                                     @RequestParam Integer amount) throws QQBusinessException {
        ResponseEntity<String> result = new ResponseEntity(CurrencyUtils.convertToWordCurrency(amount), HttpStatus.OK);
        return result;
    }

    //// ///// ////// File and document upload
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @ResponseBody
    public ResponseEntity<String> uploadFiles(@RequestParam("file") MultipartFile file, @RequestParam("documentType") String documentType,
                                              HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws Exception {
        String result = "";
        String fileFolder = uploadFileFolder.get(documentType);
        if (!file.isEmpty()) {
            try {
                FileUtils fileUtils = new FileUtils();
                result = fileUtils.uploadFile(file, "", uploadFolder + fileFolder);
            } catch (Exception e) {
                result = "Error occurred";
            }
        } else {
            result = "File was empty";
        }
        ResponseEntity<String> resultOut = new ResponseEntity(result, HttpStatus.OK);
        return resultOut;
    }

    // DOWNLOAD
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/downloadFile", produces = {"multipart/form-data"})
    public ResponseEntity<org.springframework.core.io.Resource> generateFile(@RequestParam String fileName, String documentType,
                                                                             HttpServletRequest request,
                                                                             HttpServletResponse response) throws QQBusinessException {
        String fileFolder = uploadFileFolder.get(documentType);
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext(new String[]{});
        org.springframework.core.io.Resource resource = appContext.getResource("file:" + uploadFolder + fileFolder + '/' + fileName); // new ClassPathResource("file:" + uploadFolder + fileFolder + '/'  + fileName);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=img.docx");
        ResponseEntity<org.springframework.core.io.Resource> result = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(resource);
        return result;
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/generateBarcode", produces="application/pdf")
    public ResponseEntity<org.springframework.core.io.Resource> generateBarCode(@RequestParam String serialNo, Integer fromNumber,
                                                                                Integer noOfLables,
                                                                                HttpServletRequest request,
                                                                                HttpServletResponse response) throws QQBusinessException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        barCodeService.generateBarCode(serialNo, fromNumber, noOfLables, stream);
        org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.put(HttpHeaders.CONTENT_DISPOSITION, Collections.singletonList("attachment; filename=barcode.pdf"));
        httpHeaders.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList("application/pdf"));
        ResponseEntity<org.springframework.core.io.Resource> result = ResponseEntity
                .ok()
                .headers(httpHeaders)
                .body(file);
        return result;
    }
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
//    @RequestMapping(method = RequestMethod.GET, value = "/reGenerateBarcode", produces="application/pdf")
//    public ResponseEntity<org.springframework.core.io.Resource> reGenerateBarCode(@RequestParam String serialNo, Integer fromNumber,
//                                                                                Integer noOfLables,
//                                                                                HttpServletRequest request,
//                                                                                HttpServletResponse response) throws QQBusinessException {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        barCodeService.reGenerateBarCode(serialNo, fromNumber, noOfLables, stream);
//        org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());
//        HttpHeaders httpHeaders = new HttpHeaders();
//        httpHeaders.put(HttpHeaders.CONTENT_DISPOSITION, Collections.singletonList("attachment; filename=barcode.pdf"));
//        httpHeaders.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList("application/pdf"));
//        ResponseEntity<org.springframework.core.io.Resource> result = ResponseEntity
//                .ok()
//                .headers(httpHeaders)
//                .body(file);
//        return result;
//    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/dashboardData", produces = "application/json")
    public ResponseEntity<DashboardDetails> getDashboardData(HttpServletRequest request) throws QQBusinessException {
        DashboardDetails dashboardDetails = dashboardService.getDashboardDetails();
        ResponseEntity<DashboardDetails> result = new ResponseEntity(dashboardDetails, HttpStatus.OK);
        return result;
    }

}
