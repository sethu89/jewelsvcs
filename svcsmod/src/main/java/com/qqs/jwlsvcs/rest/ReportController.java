package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.DeskReportData;
import com.qqs.jwlsvcs.api.JobTransactionReportData;
import com.qqs.jwlsvcs.api.ProductDetailReportData;
import com.qqs.jwlsvcs.api.ProductMasterReportData;
import com.qqs.jwlsvcs.service.ReportService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/report")
public class ReportController {
    @Resource
    private ReportService service;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.GET, value = "/jobTransactionReport", produces = "application/json")
    public ResponseEntity<JobTransactionReportData> getJobTransactionReportData(@RequestParam Map<String, String> searchParam,
                                                                            HttpServletRequest request) throws QQBusinessException {
        List<JobTransactionReportData> jobTransactionReportData = service.getJobTransactionReportData(searchParam);
        ResponseEntity<JobTransactionReportData> result = new ResponseEntity(jobTransactionReportData, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.GET, value = "/productMasterReport", produces = "application/json")
    public ResponseEntity<ProductMasterReportData> getProductMasterReportData(@RequestParam Map<String, String> searchParam,
                                                                              HttpServletRequest request) throws QQBusinessException {
        List<ProductMasterReportData> productMasterReportData = service.getProductMasterReportData(searchParam);
        ResponseEntity<ProductMasterReportData> result = new ResponseEntity(productMasterReportData, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.GET, value = "/productDetailReport", produces = "application/json")
    public ResponseEntity<ProductDetailReportData> getProductDetailReportData(@RequestParam Map<String, String> searchParam,
                                                                              HttpServletRequest request) throws QQBusinessException {
        List<ProductDetailReportData> productDetailReportData = service.getProductDetailReportData(searchParam);
        ResponseEntity<ProductDetailReportData> result = new ResponseEntity(productDetailReportData, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.GET, value = "/deskReport", produces = "application/json")
    public ResponseEntity<DeskReportData> getDeskReport(@RequestParam Map<String, String> searchParam,
                                                        HttpServletRequest request) throws QQBusinessException {
        List<DeskReportData> deskReportData = service.getDeskReport(searchParam);
        ResponseEntity<DeskReportData> result = new ResponseEntity(deskReportData, HttpStatus.OK);
        return result;
    }
}
