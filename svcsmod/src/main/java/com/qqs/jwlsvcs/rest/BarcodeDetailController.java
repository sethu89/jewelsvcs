package com.qqs.jwlsvcs.rest;

import com.qqs.jwlsvcs.api.BarcodeDetails;
import com.qqs.jwlsvcs.service.BarcodeDetailsService;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/barcodedetails")
public class BarcodeDetailController {
    @Resource
    BarcodeDetailsService barcodeDetailsService;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = "application/json")
    public ResponseEntity<BarcodeDetails> saveDesk(@RequestBody com.qqs.jwlsvcs.api.BarcodeDetails form) throws QQBusinessException {
        com.qqs.jwlsvcs.api.BarcodeDetails saved = barcodeDetailsService.saveBarcodeDetailsForm(form);
        ResponseEntity<BarcodeDetails> result = new ResponseEntity(saved, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "search/byproductmasterid", produces = "application/json")
    public ResponseEntity<List<BarcodeDetails>> getAllByProductMasterId(HttpServletRequest request,
                                                        @RequestParam Integer productMasterId) throws QQBusinessException {
        List<BarcodeDetails> barCodeDet = barcodeDetailsService.getAllByProductMasterId(productMasterId);
        ResponseEntity<List<BarcodeDetails>> result = new ResponseEntity(barCodeDet, HttpStatus.OK);
        return result;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_OPERATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "search/lastbarcode/byproductmasterid", produces = "application/json")
    public ResponseEntity<Integer> searchLastBarcodeByProductMasterId(HttpServletRequest request,
                                                        @RequestParam Integer productMasterId) throws QQBusinessException {
        Integer deskMaster = barcodeDetailsService.searchLastBarcodeByProductMasterId(productMasterId);
        ResponseEntity<Integer> result = new ResponseEntity(deskMaster, HttpStatus.OK);
        return result;
    }
}
