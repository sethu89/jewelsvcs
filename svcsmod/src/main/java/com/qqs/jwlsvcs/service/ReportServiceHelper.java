package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.*;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.qqs.jwlsvcs.service.translate.APITranslator.*;


@Component
public class ReportServiceHelper {
//    Logger logger = LoggerFactory.getLogger(ReportServiceHelper.class);
    @Resource
    private ReportDataService reportDataService;

    @Resource
    private JobTransactionService jobTransactionService;

    @Resource
    private DeskService deskService;

    private String parseDateString(String dateVal, String format, String timeZone, String outPutFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String formattedDateVal = "";
        if (timeZone != null && !("".equals(timeZone))) {
            sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        Date formattedDate;

        try {
            if (dateVal != null) {
                formattedDate = sdf.parse(dateVal);
                sdf = new SimpleDateFormat(outPutFormat);
                formattedDateVal = sdf.format(formattedDate);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDateVal;
    }

    public List<JobTransactionReportData> getJobTransactionReportData (Map<String, String> searchParam) throws QQBusinessException {
        List<JobTransactionReportData> jobTransactionReportDataList;

        try {
            Map<String, String> parsedParam = parseFormInput(searchParam);

            List<com.qqs.jwlsvcs.model.JobTransactionReportData> jobTransactionReportData =
                    reportDataService.getJobTransactionReportData(parsedParam.get("fromDate"), parsedParam.get("toDate"),
                            Integer.valueOf(parsedParam.get("productMasterId")), parsedParam.get("inventoryStatus"));


            if("true".equals(parsedParam.get("showOnlyLatest"))) {
                List<JobTransactionReportData> fullList = jobTransactionReportToAPI.translate(jobTransactionReportData,
                        JobTransactionReportData.class, true);
                AtomicReference<String> preBarCode = new AtomicReference<>("");
                List<JobTransactionReportData> finalJobTransactionReportDataList = new ArrayList<>();
                fullList.forEach(item -> {
                    if (!preBarCode.get().equalsIgnoreCase(item.getBarCode())) {
                        finalJobTransactionReportDataList.add(item);
                    }
                    preBarCode.set(item.getBarCode());
                });
                return finalJobTransactionReportDataList;
            } else {
                jobTransactionReportDataList = jobTransactionReportToAPI.translate(jobTransactionReportData, JobTransactionReportData.class, true);
            }

        } catch (Exception e) {
            throw new QQBusinessException("Error fetching Job Transaction Report data");
        }
        return jobTransactionReportDataList;
    }

    public List<DeskReportData> getDeskReport (Map<String, String> searchParam) throws QQBusinessException {
        List<DeskReportData> deskReportDataList;

        try {
            Map<String, String> parsedParam = parseFormInput(searchParam);

            List<com.qqs.jwlsvcs.model.DeskReportData> deskReportData =
                    reportDataService.getDeskReportData(parsedParam.get("fromDate"), parsedParam.get("toDate"),
                            Integer.valueOf(parsedParam.get("productMasterId")));

            deskReportDataList = deskReportToAPI.translate(deskReportData, DeskReportData.class, true);

        } catch (Exception e) {
            throw new QQBusinessException("Error fetching Desk Report data");
        }
        return deskReportDataList;
    }

    public List<ProductMasterReportData> getProductMasterReportData (Map<String, String> searchParam) throws QQBusinessException {
        List<ProductMasterReportData> productMasterReportDataList;

        try {
            Map<String, String> parsedParam = parseFormInput(searchParam);

            List<com.qqs.jwlsvcs.model.ProductMasterReportData> productMasterReportData =
                    reportDataService.getProductMasterReportData(parsedParam.get("fromDate"), parsedParam.get("toDate"),
                            Integer.valueOf(parsedParam.get("productMasterId")));

            productMasterReportDataList = productMasterReportToAPI.translate(productMasterReportData, ProductMasterReportData.class, true);

        } catch (Exception e) {
            throw new QQBusinessException("Error fetching Product Master Report data");
        }
        return productMasterReportDataList;
    }

    public List<ProductDetailReportData> getProductDetailReportData (Map<String, String> searchParam) throws QQBusinessException {
        List<ProductDetailReportData> productDetailReportDataList;

        try {
            Map<String, String> parsedParam = parseFormInput(searchParam);

            List<com.qqs.jwlsvcs.model.ProductDetailReportData> productDetailReportData =
                    reportDataService.getProductDetailReportData(parsedParam.get("fromDate"), parsedParam.get("toDate"),
                            Integer.valueOf(parsedParam.get("productMasterId")));

            productDetailReportDataList = productDetailReportToAPI.translate(productDetailReportData, ProductDetailReportData.class, true);

            List<Integer> productIds = new ArrayList<>();
            productDetailReportData.forEach(item -> {
                productIds.add(item.getId());
            });

            List<Desk> deskList = deskService.searchDesk(new HashMap<>(), true);
            Map<Integer, String> deskMap = new HashMap<>();
            deskList.forEach(desk -> {
                deskMap.put(desk.getId(), desk.getName());
            });

            List<JobTransaction> jobTransactions = jobTransactionService.getJobTransactionByProductIds(productIds);
            Map<Integer, List<JobTransaction>> jobTransactionMap = new HashMap<>();
            jobTransactions.forEach(jobTransaction -> {
                jobTransactionMap.computeIfAbsent(jobTransaction.getProductId(), k -> new ArrayList<>());
                jobTransaction.setDeskName(deskMap.get(jobTransaction.getDeskId()));
                jobTransactionMap.get(jobTransaction.getProductId()).add(jobTransaction);
            });

            if(jobTransactionMap.size() > 0) {
                productDetailReportDataList.forEach(item -> {
                    if(jobTransactionMap.get(item.getId()) != null) {
                        item.setJobTransactionList(jobTransactionMap.get(item.getId()));
                    }
                });
            }

        } catch (Exception e) {
            throw new QQBusinessException("Error fetching Product Detail Report data");
        }
        return productDetailReportDataList;
    }

    public List<JobTransactionData> getJobTransactionData (Map<String, String> searchParam) throws QQBusinessException {
        List<JobTransactionData> jobTransactionDataList;

        try {
            Map<String, String> parsedParam = parseFormInput(searchParam);

            List<com.qqs.jwlsvcs.model.JobTransactionData> jobTransactionData =
                    reportDataService.getJobTransactionData(parsedParam.get("fromDate"), parsedParam.get("toDate"),
                            Integer.valueOf(parsedParam.get("productMasterId")));

            jobTransactionDataList = jobTransactionDataToAPI.translate(jobTransactionData, JobTransactionData.class, true);


        } catch (Exception e) {
            throw new QQBusinessException("Error fetching Job Transaction data");
        }
        return jobTransactionDataList;
    }

    private Map<String, String> parseFormInput(Map<String, String> searchParam) {
        Map<String, String> parsedParam = new HashMap<>();
        String reportFromDate = searchParam.get("reportFromDate");
        String reportToDate = searchParam.get("reportToDate");
        String productMasterId = searchParam.get("productMasterId");
        String showOnlyLatest = searchParam.get("showOnlyLatest");
        String inventoryStatus = searchParam.get("inventoryStatus");

        if (productMasterId == null || "".equals(productMasterId)) {
            productMasterId = "0";
        }

        if (showOnlyLatest == null) {
            showOnlyLatest = "";
        }

        if (inventoryStatus == null) {
            inventoryStatus = "";
        }

        String fromDateFormat;
        String toDateFormat;
        if (reportFromDate.length() > 10) {
            fromDateFormat = "E MMM dd yyyy hh:mm:ss";
        } else {
            fromDateFormat = "yyyy-MM-dd";
        }
        if (reportToDate.length() > 10) {
            toDateFormat = "E MMM dd yyyy hh:mm:ss";
        } else {
            toDateFormat = "yyyy-MM-dd";
        }

        String fromDate = parseDateString(reportFromDate, fromDateFormat, "GMT", "yyyy-MM-dd");
        String toDate = parseDateString(reportToDate, toDateFormat, "GMT", "yyyy-MM-dd");

        parsedParam.put("productMasterId", productMasterId);
        parsedParam.put("fromDate", fromDate);
        parsedParam.put("toDate", toDate);
        parsedParam.put("inventoryStatus", inventoryStatus);
        parsedParam.put("showOnlyLatest", showOnlyLatest);
        return parsedParam;
    }

}
