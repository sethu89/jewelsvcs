package com.qqs.jwlsvcs.service.translate;

public interface CodeTranslator<T> {
    T translate(String code);
}
