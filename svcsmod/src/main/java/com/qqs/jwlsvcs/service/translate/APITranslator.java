package com.qqs.jwlsvcs.service.translate;


import com.qqs.jwlsvcs.model.*;
import com.qqs.qqsoft.utils.ApiUtils;
import com.qqs.qqsoft.utils.SearchCriteria;

public class APITranslator {


    public static ApiUtils<States, com.qqs.jwlsvcs.api.States> statesToAPI = new ApiUtils<>();

    public static ApiUtils<ProductMaster, com.qqs.jwlsvcs.api.ProductMaster> productMasterToAPI = new ApiUtils<>();
    public static ApiUtils<PropAttributes, com.qqs.jwlsvcs.api.PropAttributes> propAttributesToAPI = new ApiUtils<>();
    public static ApiUtils<JobTransaction, com.qqs.jwlsvcs.api.JobTransaction> jobTransactionToAPI = new ApiUtils<>();

    public static ApiUtils<JobTransactionReportData, com.qqs.jwlsvcs.api.JobTransactionReportData> jobTransactionReportToAPI = new ApiUtils<>();
    public static ApiUtils<ProductMasterReportData, com.qqs.jwlsvcs.api.ProductMasterReportData> productMasterReportToAPI = new ApiUtils<>();
    public static ApiUtils<ProductDetailReportData, com.qqs.jwlsvcs.api.ProductDetailReportData> productDetailReportToAPI = new ApiUtils<>();
    public static ApiUtils<DeskReportData, com.qqs.jwlsvcs.api.DeskReportData> deskReportToAPI = new ApiUtils<>();
    public static ApiUtils<JobTransactionData, com.qqs.jwlsvcs.api.JobTransactionData> jobTransactionDataToAPI = new ApiUtils<>();


    public static ApiUtils<Product, com.qqs.jwlsvcs.api.Product> productToAPI = new ApiUtils<>();


    public static ApiUtils<SearchCriteria, com.qqs.jwlsvcs.service.SearchCriteria> searchCriteriaToJPA = new ApiUtils<>();
    public static ApiUtils<CodeAssociation, com.qqs.jwlsvcs.api.common.CodeAssociation> codeAssociationToAPI = new ApiUtils<>();
    public static ApiUtils<Codes, com.qqs.jwlsvcs.api.common.Codes> codesToAPI = new ApiUtils<>();
    public static ApiUtils<Desk, com.qqs.jwlsvcs.api.Desk> deskToAPI = new ApiUtils<>();
    public static ApiUtils<BarcodeDetails, com.qqs.jwlsvcs.api.BarcodeDetails> barcodeDetailsToAPI = new ApiUtils<>();




    public static ApiUtils<com.qqs.jwlsvcs.api.ProductMaster, ProductMaster> productMasterToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.PropAttributes, PropAttributes> propAttributesToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.JobTransaction, JobTransaction> jobTransactionToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.Product, Product> productToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.common.CodeAssociation , CodeAssociation> codeAssociationToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.common.Codes , Codes> codesToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.Desk, Desk> deskToDB = new ApiUtils<>();
    public static ApiUtils<com.qqs.jwlsvcs.api.BarcodeDetails, BarcodeDetails> barcodeDetailsToDB = new ApiUtils<>();


}
