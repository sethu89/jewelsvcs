package com.qqs.jwlsvcs.service;

import com.qqs.qqsoft.BarCode.BarCodeGenerateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;


@Component
public class BarCodeService {

    Logger logger = LoggerFactory.getLogger(BarCodeService.class);

    public String generateBarCode(String serialNo, Integer fromNumber, Integer noOfLables, ByteArrayOutputStream stream) {

        BarCodeGenerateService.generateBarcode(serialNo, fromNumber, noOfLables, stream);

        return "Success Generate Barcode";
    }
}
