package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.ProductMaster;
import com.qqs.jwlsvcs.api.PropAttributes;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.SearchCriteriaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.qqs.qqsoft.utils.DateUtils;


import javax.annotation.Resource;
import java.util.*;

import static com.qqs.jwlsvcs.service.translate.APITranslator.*;

@Component
public class ProductMasterService {

    Logger logger = LoggerFactory.getLogger(ProductMasterService.class);

    @Resource
    SearchCriteriaUtils searchCriteriaUtils;

    @Resource
    DataService ds;

    public ProductMaster saveProductMaster(ProductMaster productMasterData) throws QQBusinessException {
        ProductMaster productMasterToApi = null;
        Integer loggedInUser = ds.getSecurity().getLoggedInUser();
        try {
            com.qqs.jwlsvcs.model.ProductMaster toSaveProductMaster =  productMasterToDB.translate(productMasterData, com.qqs.jwlsvcs.model.ProductMaster.class, true);

            if(toSaveProductMaster.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.ProductMaster>().setTimeStamp(toSaveProductMaster, com.qqs.jwlsvcs.model.ProductMaster.class, true);
                toSaveProductMaster.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.ProductMaster>().setTimeStamp(toSaveProductMaster, com.qqs.jwlsvcs.model.ProductMaster.class, false);
                toSaveProductMaster.setCreatedBy(loggedInUser);
            }
            com.qqs.jwlsvcs.model.ProductMaster productMaster = ds.getProductMasterDataService().saveProductMaster(toSaveProductMaster);

            List<com.qqs.jwlsvcs.model.PropAttributes> propAttributesToSave = propAttributesToDB.translate
                    (productMasterData.getPropAttributes(), com.qqs.jwlsvcs.model.PropAttributes.class, true);
            for (com.qqs.jwlsvcs.model.PropAttributes propAttributesItem : propAttributesToSave) {
                if (propAttributesItem.getId() > 0) { // update
                    new DateUtils<com.qqs.jwlsvcs.model.PropAttributes>().setTimeStamp(propAttributesItem, com.qqs.jwlsvcs.model.PropAttributes.class, true);
                    propAttributesItem.setModifiedBy(loggedInUser);
                } else { // insert
                    new DateUtils<com.qqs.jwlsvcs.model.PropAttributes>().setTimeStamp(propAttributesItem, com.qqs.jwlsvcs.model.PropAttributes.class, false);
                    propAttributesItem.setCreatedBy(loggedInUser);
                }
                propAttributesItem.setProductMasterId(productMaster.getId());
            }
            Iterable<com.qqs.jwlsvcs.model.PropAttributes> savedPropAttributes =  ds.getProductMasterDataService().savePropAttributes(propAttributesToSave);
            List<PropAttributes> savedPropAttributesAPI = propAttributesToAPI.translate(savedPropAttributes, PropAttributes.class, false);


            productMasterToApi = productMasterToAPI.translate(productMaster , ProductMaster.class, true);

            productMasterToApi.setPropAttributes(savedPropAttributesAPI);
        } catch (Exception e ) {
            System.out.println(e);
            throw new QQBusinessException("Error while saving ProductMaster details");
        }
        return productMasterToApi;

    }


    public ProductMaster getProductMasterById(Integer productMasterId) throws QQBusinessException {
        try {
            Optional<com.qqs.jwlsvcs.model.ProductMaster> productMaster = ds.getProductMasterDataService().getProductMasterById(productMasterId);
            if (!productMaster.isPresent()) {
                throw new QQBusinessException("No ProductMaster found");
            }
            ProductMaster productMasterAPI = productMasterToAPI.translate(productMaster.get(), ProductMaster.class, false);
            return productMasterAPI;
        } catch (Exception e) {
            logger.error("ProductMaster fetch error", e);
        }
        throw new QQBusinessException("ProductMaster could not be retrieved");
    }

    public ProductMaster getProductMasterByIBarCodeChar(String barCodeChar) throws QQBusinessException {
        try {
            Optional<com.qqs.jwlsvcs.model.ProductMaster> productMaster = ds.getProductMasterDataService().getProductMasterByBarCodeChar(barCodeChar);
            if (!productMaster.isPresent()) {
                throw new QQBusinessException("No ProductMaster found");
            }
            ProductMaster productMasterAPI = productMasterToAPI.translate(productMaster.get(), ProductMaster.class, false);
            return productMasterAPI;
        } catch (Exception e) {
            logger.error("ProductMaster fetch error", e);
        }
        throw new QQBusinessException("ProductMaster could not be retrieved");
    }

    public List<PropAttributes> getPropAttributesByProductMasterId(Integer productMasterId) {
        List<PropAttributes> propAttributes = new ArrayList<>();
        Optional<List<com.qqs.jwlsvcs.model.PropAttributes>> propAttributesList = ds.getProductMasterDataService().getPropAttributesByProductMasterId(productMasterId);
        if(propAttributesList.isPresent()) {
            try {
                propAttributes = propAttributesToAPI.translate(propAttributesList.get(), PropAttributes.class, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
        return propAttributes;
    }

    public List<ProductMaster> getAllProductMaster() throws QQBusinessException {
        List<ProductMaster> result = new ArrayList<>();
        Iterable<com.qqs.jwlsvcs.model.ProductMaster> countries = ds.getProductMasterDataService().getAllProductMaster();
        countries.forEach(productMaster -> {
            try {
                result.add(productMasterToAPI.translate(productMaster, ProductMaster.class, false));
            } catch (Exception e) {
                logger.error("translation exception");
            }
        });
        return result;
    }





    public List<ProductMaster> searchProductMaster(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        List<SearchCriteria> conditions = createProductMasterSearchCriteria(params, exactMatch);
        Optional<List<com.qqs.jwlsvcs.model.ProductMaster>> productMasterList = ds.getProductMasterDataService().searchProductMaster(conditions);
        if (!productMasterList.isPresent())
            throw new QQBusinessException("No client found for criteria");
        List<ProductMaster> result = null;
        try {
            result = productMasterToAPI.translate(productMasterList.get(), ProductMaster.class, false);
        } catch (Exception e) {
            logger.error("translation exception");
        }
        return result;
    }

    private List<SearchCriteria> createProductMasterSearchCriteria(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        Set validColumns = new HashSet(Arrays.asList(new String[]{"name", "sortname", "barCodeChar", "status"}));
        params.remove("exactMatch");
        List<SearchCriteria> conditions = new ArrayList<>();
        try {
            conditions = searchCriteriaToJPA.translate(
                    searchCriteriaUtils.createSearchCriteria(params, exactMatch, null, validColumns),
                    SearchCriteria.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conditions;
    }



}
