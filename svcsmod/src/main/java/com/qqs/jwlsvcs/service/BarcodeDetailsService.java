package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.BarcodeDetails;
import com.qqs.jwlsvcs.api.Product;
import com.qqs.jwlsvcs.utils.Constants;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import static com.qqs.jwlsvcs.service.translate.APITranslator.*;


@Component
public class BarcodeDetailsService {
    @Resource
    DataService ds;

    @Resource
    ProductService productService;

    Logger logger = LoggerFactory.getLogger(BarCodeService.class);

    public List<BarcodeDetails> getAllByProductMasterId(Integer productMasterId) throws QQBusinessException {
        try {
            Optional<List<com.qqs.jwlsvcs.model.BarcodeDetails>> productMaster = ds.getBarcodeDetailsDataService().getAllByProductMasterId(productMasterId);
            if (!productMaster.isPresent()) {
                return null;
//                throw new QQBusinessException("No Barcode detail found");
            }
            List<BarcodeDetails> barcodeDetailsAPI = barcodeDetailsToAPI.translate(productMaster.get(), BarcodeDetails.class, false);
            return barcodeDetailsAPI;
        } catch (Exception e) {
            logger.error("Barcode detail fetch error", e);
        }
        throw new QQBusinessException("Barcode detail could not be retrieved");
    }

    @Transactional
    public BarcodeDetails saveBarcodeDetailsForm(BarcodeDetails source) throws QQBusinessException {

        BarcodeDetails barcodeDetailsToApi = null;

        var firstNumber = source.getFromNumber();
        var lastNumber  = source.getFromNumber() + source.getNoOfLables();
        String startCodeNumber = "";
        startCodeNumber = String.format("%06d", firstNumber);

        String endCodeNumber = "";
        endCodeNumber = String.format("%06d", lastNumber - 1);

        source.setStartCode(source.getSeriesNo() + startCodeNumber);
        source.setEndCode(source.getSeriesNo() + endCodeNumber);

        source.setGeneratedDate(new Date());

        for(int aw = source.getFromNumber(); aw <  lastNumber; ++aw) {
            String digit = "";
            digit = String.format("%06d", aw);
            System.out.println(source.getSeriesNo() + digit);
            Product productDetail = new Product();
            productDetail.setBarCode(source.getSeriesNo() + digit);
            productDetail.setProcessStatus(Constants.PRODUCT_STATUS_INACTIVE);
            productDetail.setProductStatus(Constants.PRODUCT_STATUS_INACTIVE);
            productDetail.setProductMasterId(source.getProductMasterId());
            productService.saveProduct(productDetail);
        }

        Integer loggedInUser = ds.getSecurity().getLoggedInUser();
        try {
            com.qqs.jwlsvcs.model.BarcodeDetails barCodeDetailForm = barcodeDetailsToDB.translate(source, com.qqs.jwlsvcs.model.BarcodeDetails.class, true);
            if(barCodeDetailForm.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.BarcodeDetails>().setTimeStamp(barCodeDetailForm, com.qqs.jwlsvcs.model.BarcodeDetails.class, true);
                barCodeDetailForm.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.BarcodeDetails>().setTimeStamp(barCodeDetailForm, com.qqs.jwlsvcs.model.BarcodeDetails.class, false);
                barCodeDetailForm.setCreatedBy(loggedInUser);
            }

            com.qqs.jwlsvcs.model.BarcodeDetails barCodeDetails = ds.getBarcodeDetailsDataService().saveBarCodeDetails(barCodeDetailForm);

            barcodeDetailsToApi = barcodeDetailsToAPI.translate(barCodeDetails, BarcodeDetails.class, true);
        } catch (Exception e) {
            logger.error("Save error ", e);
            throw new QQBusinessException("BarcodeDetails   save error", e);
        }
        return barcodeDetailsToApi;
    }

    public Integer searchLastBarcodeByProductMasterId(Integer productMasterId) throws QQBusinessException {
        Optional<String> latestBarCodeValue = ds.getBarcodeDetailsDataService().getLastDetailsByProductMasterId(productMasterId);

        if (!latestBarCodeValue.isEmpty()) {
            System.out.println(latestBarCodeValue);
            String latestCode = latestBarCodeValue.get().substring(latestBarCodeValue.get().length() - 6);
            return  Integer.valueOf(latestCode) + 1;
        } else {
            return 1;
        }
    }

}
