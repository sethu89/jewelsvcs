package com.qqs.jwlsvcs.service.startup;

import com.qqs.jwlsvcs.model.CodeAssociation;
import com.qqs.jwlsvcs.model.Codes;
import com.qqs.jwlsvcs.service.CodesDataService;
import com.qqs.qqsoft.utils.ApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Component
public class ApplicationCodeMap {
    private Map<String, Map<String, com.qqs.jwlsvcs.api.common.Codes>> codeMap = new HashMap<>(100);
    private Map<String, Map<String, com.qqs.jwlsvcs.api.common.Codes>> pickListMap = new HashMap<>(100);
    Logger logger = LoggerFactory.getLogger(ApplicationCodeMap.class);
    @Resource
    private CodesDataService codesDataService;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        this.fillCodeData();
    }

    public void fillCodeData() {
        Iterable<Codes> codes = codesDataService.getAllCodes();
        Iterable<CodeAssociation> associations = codesDataService.getAllCodeAssociation();
        Map<String, CodeAssociation> associationMap = new HashMap<>();
        for (CodeAssociation c : associations) {
            associationMap.put(c.getCategory(), c);
        }
        Map<String, com.qqs.jwlsvcs.api.common.Codes> categoryMap;
        ApiUtils<Codes, com.qqs.jwlsvcs.api.common.Codes> codeTranslator =
                new ApiUtils();
        for (Codes c : codes) {
            categoryMap = codeMap.get(c.getCategory());
            if (categoryMap == null) {
                categoryMap = new HashMap<>(10);
                codeMap.put(c.getCategory(), categoryMap);
            }
            try {
                com.qqs.jwlsvcs.api.common.Codes codeApi = codeTranslator.translate(c, com.qqs.jwlsvcs.api.common.Codes.class, false);
                categoryMap.put(c.getCode(), codeApi);
                fillPickListValues(associationMap, codeApi);
            } catch (Exception e) {
                logger.error("Code translation error", e);
            }
        }
    }

    // Fill pickList codes based on association
    private void fillPickListValues(Map<String, CodeAssociation> associationMap, com.qqs.jwlsvcs.api.common.Codes codeApi) {
        if (associationMap.get(codeApi.getCategory()) != null &&
                associationMap.get(codeApi.getCategory()).getPicklist().equalsIgnoreCase("Y")) {
            Map<String, com.qqs.jwlsvcs.api.common.Codes> pickListValues = pickListMap.get(codeApi.getCategory());
            if (pickListValues == null) {
                pickListValues = new HashMap<>(10);
                pickListMap.put(codeApi.getCategory(), pickListValues);
            }
            pickListValues.put(codeApi.getCode(), codeApi);
        }
    }

    public Map<String, Map<String, com.qqs.jwlsvcs.api.common.Codes>> getCodeMap() {
        return codeMap;
    }

    public Map<String, Map<String, com.qqs.jwlsvcs.api.common.Codes>> getPickListMap() {
        return pickListMap;
    }

    public Map<String, com.qqs.jwlsvcs.api.common.Codes> getCodesForCategory(String category) {
        return codeMap.get(category);
    }
}
