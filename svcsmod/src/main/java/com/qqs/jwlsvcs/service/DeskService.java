package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.Desk;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.DateUtils;
import com.qqs.qqsoft.utils.SearchCriteriaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.*;
import static com.qqs.jwlsvcs.service.translate.APITranslator.*;

@Component
public class DeskService {
    Logger logger = LoggerFactory.getLogger(DeskService.class);

    @Resource
    DataService ds;
    @Resource
    SearchCriteriaUtils searchCriteriaUtils;
    

    @Transactional
    public Desk saveDeskForm(Desk source) throws QQBusinessException {

        Desk deskDataToAPI = null;

        Integer loggedInUser = ds.getSecurity().getLoggedInUser();
        try {
            com.qqs.jwlsvcs.model.Desk deskForm = deskToDB.translate(source, com.qqs.jwlsvcs.model.Desk.class, true);
            if(deskForm.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.Desk>().setTimeStamp(deskForm, com.qqs.jwlsvcs.model.Desk.class, true);
                deskForm.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.Desk>().setTimeStamp(deskForm, com.qqs.jwlsvcs.model.Desk.class, false);
                deskForm.setCreatedBy(loggedInUser);
            }

            com.qqs.jwlsvcs.model.Desk ndaData = ds.getDeskDataService().saveDesk(deskForm);

            deskDataToAPI = deskToAPI.translate(ndaData, Desk.class, true);
        } catch (Exception e) {
            logger.error("Save error ", e);
            throw new QQBusinessException("desk   save error", e);
        }
        return deskDataToAPI;
    }

    public Desk getDeskById(Integer id) throws QQBusinessException {
        Desk deskDataToAPI = null;
        try {
            Optional<com.qqs.jwlsvcs.model.Desk> saved = ds.getDeskDataService().findDeskById(id);
            deskDataToAPI = deskToAPI.translate(saved.get(), Desk.class, true);
        } catch (Exception e) {
            logger.error("Save error ", e);
            throw new QQBusinessException("desk   retrive error", e);
        }
        return deskDataToAPI;
    }

    public List<Desk> searchDesk(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        List<SearchCriteria> conditions = createSearchCriteria(params, exactMatch);
        Optional<List<com.qqs.jwlsvcs.model.Desk>> deskForm = ds.getDeskDataService().SearchDesk(conditions);
        if (!deskForm.isPresent())
            throw new QQBusinessException("No desk  found for criteria");
        List<Desk> deskList = null;
        try {
            deskList = deskToAPI.translate(deskForm.get(), Desk.class, false);
        } catch (Exception e) {
            logger.error("Translation failed", e);
        }
        return deskList;
    }

    private List<SearchCriteria> createSearchCriteria(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        Set validColumns = new HashSet(Arrays.asList(new String[]{"name", "status", "deskType"}));
        Map<String, String> operators = new HashMap<>(2);
        params.remove("exactMatch");
        List<SearchCriteria> conditions = new ArrayList<>();
        try {
            conditions = searchCriteriaToJPA.translate(
                    searchCriteriaUtils.createSearchCriteria(params, exactMatch, operators, validColumns),
                    SearchCriteria.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conditions;
    }


}
