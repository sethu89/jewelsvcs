package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.common.CodeAssociation;
import com.qqs.jwlsvcs.api.common.Codes;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.DateUtils;
import com.qqs.qqsoft.utils.SearchCriteriaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

import static com.qqs.jwlsvcs.service.translate.APITranslator.*;

@Component
public class CodesService {

    Logger logger = LoggerFactory.getLogger(CodesService.class);

    @Resource
    DataService ds;

    @Resource
    CodesDataService codesDataService;

    @Resource
    SearchCriteriaUtils searchCriteriaUtils;

    public CodeAssociation saveCodeAssociation(CodeAssociation codeAssociation) throws QQBusinessException {
        CodeAssociation codeAssociationApi = null;
        Integer loggedInUser = ds.getSecurity().getLoggedInUser();

        try {
            com.qqs.jwlsvcs.model.CodeAssociation toSave =  codeAssociationToDB.translate(codeAssociation, com.qqs.jwlsvcs.model.CodeAssociation.class, true);
            if(toSave.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.CodeAssociation>().setTimeStamp(toSave, com.qqs.jwlsvcs.model.CodeAssociation.class, true);
                toSave.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.CodeAssociation>().setTimeStamp(toSave, com.qqs.jwlsvcs.model.CodeAssociation.class, false);
                toSave.setCreatedBy(loggedInUser);
            }
            com.qqs.jwlsvcs.model.CodeAssociation codeAssociationDB = codesDataService.saveCodeAssociation(toSave);

            codeAssociationApi = codeAssociationToAPI.translate(codeAssociationDB , CodeAssociation.class, true);

        } catch (Exception e ) {
            System.out.println(e);
            throw new QQBusinessException("Error while Saving Code Association");
        }
        return codeAssociationApi;

    }

    public CodeAssociation getCodeAssociationById(Integer id) throws QQBusinessException {
        Optional<com.qqs.jwlsvcs.model.CodeAssociation> codeAssociation = codesDataService.getCodeAssociationById(id);
        try {
            if (codeAssociation.isPresent()) {
                return codeAssociationToAPI.translate(codeAssociation.get(), CodeAssociation.class, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new QQBusinessException("No Code Association information found");
    }

    public List<CodeAssociation> searchCodeAssociations(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        List<SearchCriteria> conditions = createSearchCriteria(params, exactMatch);
        Optional<List<com.qqs.jwlsvcs.model.CodeAssociation>> codeAssociationList = codesDataService.searchCodeAssoication(conditions);
        if (!codeAssociationList.isPresent())
            throw new QQBusinessException("No Code Association found for selected search criteria");
        try {
            return codeAssociationToAPI.translate(codeAssociationList.get(), CodeAssociation.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new QQBusinessException("No Code Association information found");
    }

    private List<SearchCriteria> createSearchCriteria(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        Set validColumns = new HashSet(Arrays.asList(new String[]{"category", "picklist"}));
        Map<String, String> operators = new HashMap<>(2);
        params.remove("exactMatch");
        List<SearchCriteria> conditions = new ArrayList<>();
        try {
            conditions = searchCriteriaToJPA.translate(
                    searchCriteriaUtils.createSearchCriteria(params, exactMatch, operators, validColumns),
                    SearchCriteria.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conditions;
    }

    public Codes saveCodes(Codes codes) throws QQBusinessException {
        Codes codesAPI = null;
        Integer loggedInUser = ds.getSecurity().getLoggedInUser();

        try {
            com.qqs.jwlsvcs.model.Codes toSave =  codesToDB.translate(codes, com.qqs.jwlsvcs.model.Codes.class, true);
            if(toSave.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.Codes>().setTimeStamp(toSave, com.qqs.jwlsvcs.model.Codes.class, true);
                toSave.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.Codes>().setTimeStamp(toSave, com.qqs.jwlsvcs.model.Codes.class, false);
                toSave.setCreatedBy(loggedInUser);
            }
            com.qqs.jwlsvcs.model.Codes codesDB = codesDataService.saveCodes(toSave);

            codesAPI = codesToAPI.translate(codesDB , Codes.class, true);

        } catch (Exception e ) {
            System.out.println(e);
            throw new QQBusinessException("Error while Saving Codes");
        }
        return codesAPI;

    }

    public Codes getCodesById(Integer id) throws QQBusinessException {
        Optional<com.qqs.jwlsvcs.model.Codes> codes = codesDataService.getCodesById(id);
        try {
            if (codes.isPresent()) {
                return codesToAPI.translate(codes.get(), Codes.class, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new QQBusinessException("No Codes information found");
    }

    public List<Codes> searchCodes(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        List<SearchCriteria> conditions = createCodesSearchCriteria(params, exactMatch);
        Optional<List<com.qqs.jwlsvcs.model.Codes>> codesList = codesDataService.searchCodes(conditions);
        if (!codesList.isPresent())
            throw new QQBusinessException("No Code Association found for selected search criteria");
        try {
            return codesToAPI.translate(codesList.get(), Codes.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new QQBusinessException("No Codes information found");
    }

    private List<SearchCriteria> createCodesSearchCriteria(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        Set validColumns = new HashSet(Arrays.asList(new String[]{"category", "code"}));
        Map<String, String> operators = new HashMap<>(2);
        params.remove("exactMatch");
        List<SearchCriteria> conditions = new ArrayList<>();
        try {
            conditions = searchCriteriaToJPA.translate(
                    searchCriteriaUtils.createSearchCriteria(params, exactMatch, operators, validColumns),
                    SearchCriteria.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conditions;
    }

}
