package com.qqs.jwlsvcs.service.startup;

import com.qqs.qqsoft.QQBusinessException;
//import com.qqsvcs.service.IdleTimeService;
import com.qqs.qqsoft.mail.MailService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Component
@EnableScheduling
@EnableAsync
public class ApplicationTaskScheduler {


    //    private Map<String, Map<String, com.qqs.jwlsvcs.api.common.Codes>> codeMap = new HashMap<>(100);
//    private Map<String, Map<String, com.qqs.jwlsvcs.api.common.Codes>> pickListMap = new HashMap<>(100);
    Logger logger = LoggerFactory.getLogger(ApplicationTaskScheduler.class);

//    @EventListener
//    public void onApplicationEvent(ContextRefreshedEvent event) throws QQBusinessException {
//        startScheduledJobs();
//    }

    //    "0 0 18 * * MON-FRI" means every weekday at 6:00 PM.
//
//"0 0 */1 * * *" means every hour on the hour.
//
//            "0 0 */8 * * *" means every 8 hours on the hour.
//
//            "0 0 12 1 * *" means 12:00 PM on the first day of every month.
//    @Scheduled(fixedRate = 600000) // 600, 000 for every mins  10 mins
//    @Scheduled(fixedRate = 6000000) // 600, 000 for every mins  10 mins
//    @Scheduled(cron = "0 0 10 * * MON") //  1 Day
//    public void startScheduledJobs() throws QQBusinessException {
//        DateTime now = new DateTime(); // Now
//        logger.info("Scheduler Called at:" + now);
//        reportServiceHelper.generatePendingOrderMail();
//    }

}
