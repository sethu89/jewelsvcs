package com.qqs.jwlsvcs.service;


import com.qqs.qqsoft.utils.SecurityUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class DataService {
    @Resource
    private ProductMasterDataService productMasterDataService;

    @Resource
    private DeskDataService deskDataService;

    @Resource
    private JobTransactionDataService  jobTransactionDataService;

    @Resource
    private ProductDataService productDataService;
    @Resource
    private BarcodeDetailsDataService barcodeDetailsDataService;

    @Resource
    SecurityUtils security;

    public SecurityUtils getSecurity() {
        return security;
    }

    public ProductMasterDataService getProductMasterDataService() { return productMasterDataService; }


    public DeskDataService getDeskDataService() { return deskDataService; }


    public JobTransactionDataService getJobTransactionDataService() { return jobTransactionDataService; }

    public ProductDataService getProductDataService() { return productDataService; }

    public BarcodeDetailsDataService getBarcodeDetailsDataService() { return barcodeDetailsDataService; }
}
