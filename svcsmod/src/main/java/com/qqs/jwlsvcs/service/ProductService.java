package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.*;
import com.qqs.jwlsvcs.model.UserEntity;
import com.qqs.jwlsvcs.utils.JwlUtils;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.DateUtils;
import com.qqs.qqsoft.utils.SearchCriteriaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

import static com.qqs.jwlsvcs.service.translate.APITranslator.*;
import static com.qqs.qqsoft.utils.DateUtils.getCurrentTime;

@Component
public class ProductService {
    Logger logger = LoggerFactory.getLogger(ProductService.class);

    @Resource
    SearchCriteriaUtils searchCriteriaUtils;

    @Resource
    ProductMasterService productMasterService;

    @Resource
    DeskService deskService;

    @Resource
    DataService ds;

    @Resource
    JobTransactionService jobTransactionService;

    @Resource
    ProductDataService productDataService;

    @Resource
    UserDataService userDataService;

    public Product saveProduct(Product productData) throws QQBusinessException {
        Product productApi;
        Integer loggedInUser = ds.getSecurity().getLoggedInUser();
        try {
            com.qqs.jwlsvcs.model.Product toSaveJobTrans =  productToDB.translate(productData, com.qqs.jwlsvcs.model.Product.class, true);

            if(toSaveJobTrans.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.Product>().setTimeStamp(toSaveJobTrans, com.qqs.jwlsvcs.model.Product .class, true);
                toSaveJobTrans.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.Product>().setTimeStamp(toSaveJobTrans, com.qqs.jwlsvcs.model.Product.class, false);
                toSaveJobTrans.setCreatedBy(loggedInUser);
            }

            if (productData.getFinalStatus() != null) {
                toSaveJobTrans.setFinalStatusUserId(loggedInUser);
                toSaveJobTrans.setFinalStatusDt(toSaveJobTrans.getModifiedDt());
            }
            com.qqs.jwlsvcs.model.Product product = productDataService.saveProduct(toSaveJobTrans);

            productApi = productToAPI.translate(product , Product.class, true);
            
        } catch (Exception e ) {
            throw new QQBusinessException("Error while saving Job Transaction details");
        }
        return productApi;

    }

    public Product saveProductTrans(Product productData) throws QQBusinessException {
        Product productApi;
        try {
            productApi = saveProduct(productData);
            Integer productId = productApi.getId();

            JobTransaction jobTransaction = productData.getJobTransactionList().get(0);
            jobTransaction.setProductId(productId);

            Optional<UserEntity> userEntity = userDataService.getUserById(jobTransaction.getUserId());
            if(userEntity.isPresent()) {
                jobTransaction.setDeskId(userEntity.get().getDeskId());

                // Validation to update stoneCnt for first transaction from Setter Desk
                Desk desk = deskService.getDeskById(userEntity.get().getDeskId());
                jobTransaction.setStoneCnt(0);
                if("STR".equalsIgnoreCase(desk.getDeskType()) && productData.getProductMaster() != null
                        && "OUT".equalsIgnoreCase(jobTransaction.getJobType())) {
                    String noOfStoneStr = productData.getProductMaster().getPropValue("PLNS");
                    if(!"".equals(noOfStoneStr)) {
                        jobTransaction.setStoneCnt(Integer.parseInt(noOfStoneStr));

                        List<JobTransaction> jobTransactions = jobTransactionService.getJobTransactionByProductId(productId);
                        if (jobTransactions != null && jobTransactions.size() > 0) {
                            jobTransactions.forEach(item -> {
                                if (item.getStoneCnt() != null && item.getStoneCnt() > 0) {
                                    jobTransaction.setStoneCnt(0);
                                }
                            });
                        }
                    }
                }
            }
            jobTransaction.setInTime(getCurrentTime());

            jobTransactionService.saveJobTransaction(jobTransaction);

        } catch (Exception e ) {
            throw new QQBusinessException("Error while saving Job Transaction details");
        }
        return productApi;

    }

    public Product getProductById(Integer productId) throws QQBusinessException {
        try {
            Optional<com.qqs.jwlsvcs.model.Product> product = productDataService.getProduct(productId);
            if (product.isEmpty()) {
                throw new QQBusinessException("No Product found");
            }

            return productToAPI.translate(product.get(), Product.class, false);
        } catch (Exception e) {
            logger.error("Product fetch error", e);
        }
        throw new QQBusinessException("Product could not be retrieved");
    }

    public Product getProductByBarCode(String barCode, Boolean latestTran) throws QQBusinessException {
        try {
            Optional<com.qqs.jwlsvcs.model.Product> product = productDataService.getProductyBarCode(barCode);
            Product productAPI = new Product();
            if (product.isEmpty()) {
                String barCodeChar = JwlUtils.extractBarCodeChar(barCode);
                if(!("".equals(barCodeChar))) {
                    ProductMaster productMaster = productMasterService.getProductMasterByIBarCodeChar(barCodeChar);
                    productAPI.setProductMaster(productMaster);
                    productAPI.setProductMasterId(productMaster.getId());
                }
            } else {
                productAPI = productToAPI.translate(product.get(), Product.class, false);

                productAPI.setProductMaster(productMasterService.getProductMasterById(productAPI.getProductMasterId()));
                // Fetching transaction
                List<JobTransaction> jobTransactions = jobTransactionService.getJobTransactionByProductId(productAPI.getId());
                if(jobTransactions != null && jobTransactions.size() > 0) {
                    if(latestTran) {
                        jobTransactions.sort(Comparator.comparing(JobTransaction::getInTime).reversed());
                        jobTransactions = jobTransactions.subList(0,1);
                        // ToDo date conversion to be checked and below code to be removed
                        jobTransactions.get(0).setInTime(null);
                    }
                    productAPI.setJobTransactionList(jobTransactions);
                    productAPI.setToDisplayWeight(false);

                    Integer loggedInUser = ds.getSecurity().getLoggedInUser();
                    Optional<UserEntity> userEntity = userDataService.getUserById(loggedInUser);
                    if(userEntity.isPresent()) {
                        Desk desk = deskService.getDeskById(userEntity.get().getDeskId());
                        if("GSM".equalsIgnoreCase(desk.getDeskType())) {
                            productAPI.setToDisplayWeight(true);
                            for (JobTransaction item : jobTransactions) {
                                if (item.getProdWeight() != null && item.getProdWeight() > 0.00) {
                                    productAPI.setToDisplayWeight(false);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (productAPI.getProductMasterId() != null && productAPI.getProductMasterId() > 0) {
                List<PropAttributes> propAttributes = productMasterService.getPropAttributesByProductMasterId(productAPI.getProductMasterId());
                productAPI.getProductMaster().setPropAttributes(propAttributes);
            }

            return productAPI;
        } catch (Exception e) {
            logger.error("Product fetch error", e);
        }
        throw new QQBusinessException("Product could not be retrieved");
    }

    public List<Product> searchProduct(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        List<SearchCriteria> conditions = createProductSearchCriteria(params, exactMatch);
        Optional<List<com.qqs.jwlsvcs.model.Product>> productList = productDataService.searchProduct(conditions);
        if (productList.isEmpty())
            throw new QQBusinessException("No client found for criteria");
        List<Product> result = null;
        try {
            result = productToAPI.translate(productList.get(), Product.class, false);
        } catch (Exception e) {
            logger.error("translation exception");
        }
        return result;
    }

    public Integer searchBarcodeByProductMasterId(Integer productMasterId) throws QQBusinessException {
         Optional<String> latestBarCodeValue = productDataService.searchBarcodeByProductMasterId(productMasterId);

        if (latestBarCodeValue.isPresent()) {
            System.out.println(latestBarCodeValue);
            String latestCode = latestBarCodeValue.get().substring(latestBarCodeValue.get().length() - 6);
            return  Integer.parseInt(latestCode) + 1;
        } else {
            return 1;
        }
    }

    public List<Product> searchProductsByProductMasterId(Integer productMasterId) throws QQBusinessException {
        List<Product> productArrayList = new ArrayList<>();
         Optional<List<com.qqs.jwlsvcs.model.Product>> productFromDb  =  productDataService.searchProductsByProductMasterId(productMasterId);

        if (productFromDb.isEmpty())
            return null;
        try {
            productArrayList = productToAPI.translate(productFromDb.get(), Product.class, false);
        } catch (Exception e) {
            logger.error("translation exception");
        }

        return productArrayList;
    }

    private List<SearchCriteria> createProductSearchCriteria(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        Set validColumns = new HashSet(Arrays.asList(new String[]{"userId", "productId", "deskId", "inTime", "outTime", "jobType"}));
        params.remove("exactMatch");
        List<SearchCriteria> conditions = new ArrayList<>();
        try {
            conditions = searchCriteriaToJPA.translate(
                    searchCriteriaUtils.createSearchCriteria(params, exactMatch, null, validColumns),
                    SearchCriteria.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conditions;
    }
}
