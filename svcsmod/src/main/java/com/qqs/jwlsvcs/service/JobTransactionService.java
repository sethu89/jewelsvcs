package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.JobTransaction;
import com.qqs.qqsoft.QQBusinessException;
import com.qqs.qqsoft.utils.DateUtils;
import com.qqs.qqsoft.utils.SearchCriteriaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

import static com.qqs.jwlsvcs.service.translate.APITranslator.*;

@Component
public class JobTransactionService {
    Logger logger = LoggerFactory.getLogger(JobTransactionService.class);

    @Resource
    SearchCriteriaUtils searchCriteriaUtils;

    @Resource
    DataService ds;

    public JobTransaction saveJobTransaction(JobTransaction jobTransactionData) throws QQBusinessException {
        JobTransaction jobTransactionApi = null;
        Integer loggedInUser = ds.getSecurity().getLoggedInUser();
        try {
            com.qqs.jwlsvcs.model.JobTransaction toSaveJobTrans =  jobTransactionToDB.translate(jobTransactionData, com.qqs.jwlsvcs.model.JobTransaction.class, true);

            if(toSaveJobTrans.getId() > 0) {
                new DateUtils<com.qqs.jwlsvcs.model.JobTransaction>().setTimeStamp(toSaveJobTrans, com.qqs.jwlsvcs.model.JobTransaction .class, true);
                toSaveJobTrans.setModifiedBy(loggedInUser);
            } else {
                new DateUtils<com.qqs.jwlsvcs.model.JobTransaction>().setTimeStamp(toSaveJobTrans, com.qqs.jwlsvcs.model.JobTransaction.class, false);
                toSaveJobTrans.setCreatedBy(loggedInUser);
            }
            com.qqs.jwlsvcs.model.JobTransaction jobTransaction = ds.getJobTransactionDataService().saveJobTransaction(toSaveJobTrans);

            jobTransactionApi = jobTransactionToAPI.translate(jobTransaction , JobTransaction.class, true);
            
        } catch (Exception e ) {
            System.out.println(e);
            throw new QQBusinessException("Error while saving Job Transaction details");
        }
        return jobTransactionApi;

    }

    public JobTransaction getJobTransactionById(Integer jobTransactionId) throws QQBusinessException {
        try {
            Optional<com.qqs.jwlsvcs.model.JobTransaction> jobTransaction = ds.getJobTransactionDataService().getJobTransaction(jobTransactionId);
            if (!jobTransaction.isPresent()) {
                throw new QQBusinessException("No JobTransaction found");
            }
            JobTransaction jobTransactionAPI = jobTransactionToAPI.translate(jobTransaction.get(), JobTransaction.class, false);
            return jobTransactionAPI;
        } catch (Exception e) {
            logger.error("JobTransaction fetch error", e);
        }
        throw new QQBusinessException("JobTransaction could not be retrieved");
    }

    public List<JobTransaction> getJobTransactionByProductId(Integer productId) throws QQBusinessException {
        try {
            List<com.qqs.jwlsvcs.model.JobTransaction> jobTransaction = ds.getJobTransactionDataService().getJobTransactionByProductId(productId);

            List<JobTransaction> jobTransactionList = jobTransactionToAPI.translate(jobTransaction, JobTransaction.class, false);
            return jobTransactionList;
        } catch (Exception e) {
            logger.error("JobTransaction fetch error", e);
        }
        throw new QQBusinessException("JobTransaction could not be retrieved");
    }

    public List<JobTransaction> getJobTransactionByProductIds(List<Integer> productIds) throws QQBusinessException {
        try {
            List<com.qqs.jwlsvcs.model.JobTransaction> jobTransaction = ds.getJobTransactionDataService().getJobTransactionByProductIds(productIds);

            List<JobTransaction> jobTransactionList = jobTransactionToAPI.translate(jobTransaction, JobTransaction.class, false);
            return jobTransactionList;
        } catch (Exception e) {
            logger.error("JobTransaction fetch error", e);
        }
        throw new QQBusinessException("JobTransaction could not be retrieved");
    }

    public List<JobTransaction> searchJobTransaction(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        List<SearchCriteria> conditions = createJobTransactionSearchCriteria(params, exactMatch);
        Optional<List<com.qqs.jwlsvcs.model.JobTransaction>> jobTransactionList = ds.getJobTransactionDataService().searchJobTransaction(conditions);
        if (!jobTransactionList.isPresent())
            throw new QQBusinessException("No client found for criteria");
        List<JobTransaction> result = null;
        try {
            result = jobTransactionToAPI.translate(jobTransactionList.get(), JobTransaction.class, false);
        } catch (Exception e) {
            logger.error("translation exception");
        }
        return result;
    }

    private List<SearchCriteria> createJobTransactionSearchCriteria(Map<String, String> params, Boolean exactMatch) throws QQBusinessException {
        Set validColumns = new HashSet(Arrays.asList(new String[]{"userId", "productId", "deskId", "inTime", "outTime", "jobType"}));
        params.remove("exactMatch");
        List<SearchCriteria> conditions = new ArrayList<>();
        try {
            conditions = searchCriteriaToJPA.translate(
                    searchCriteriaUtils.createSearchCriteria(params, exactMatch, null, validColumns),
                    SearchCriteria.class, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conditions;
    }



}
