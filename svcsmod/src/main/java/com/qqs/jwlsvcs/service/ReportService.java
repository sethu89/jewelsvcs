package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.api.*;
import com.qqs.qqsoft.QQBusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
public class ReportService {
    Logger logger = LoggerFactory.getLogger(ReportService.class);

    @Resource
    private ReportServiceHelper helper;

    public List<JobTransactionReportData> getJobTransactionReportData(Map<String, String> searchParam) throws QQBusinessException {
        return helper.getJobTransactionReportData(searchParam);
    }

    public List<JobTransactionData> getJobTransactionData(Map<String, String> searchParam) throws QQBusinessException {
        return helper.getJobTransactionData(searchParam);
    }

    public List<ProductMasterReportData> getProductMasterReportData(Map<String, String> searchParam) throws QQBusinessException {
        return helper.getProductMasterReportData(searchParam);
    }

    public List<ProductDetailReportData> getProductDetailReportData(Map<String, String> searchParam) throws QQBusinessException {
        return helper.getProductDetailReportData(searchParam);
    }


    public List<DeskReportData> getDeskReport(Map<String, String> searchParam) throws QQBusinessException {
        return helper.getDeskReport(searchParam);
    }
}
