package com.qqs.jwlsvcs.service.translate;

import org.apache.commons.lang3.StringUtils;

public class PartTranslator {
    public enum PartCategory implements CodeTranslator<PartCategory> {
        Casting("CST"), Machining("MCN"), Assembly("ALY");
        private String code;

        PartCategory(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        @Override
        public PartCategory translate(String code) {
            for (PartCategory s : PartCategory.values()) {
                if (StringUtils.equalsIgnoreCase(s.name(), code)) {
                    return s;
                }
            }
            return null;
        }
    }

    public enum WeightUnit implements CodeTranslator<WeightUnit> {
        KiloGram("KG"), Gram("GM"), MilliGram("MG"), Tonne("TN");
        private String code;

        WeightUnit(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        @Override
        public WeightUnit translate(String code) {
            for (WeightUnit s : WeightUnit.values()) {
                if (StringUtils.equalsIgnoreCase(s.name(), code)) {
                    return s;
                }
            }
            return null;
        }
    }

    public enum PartStage implements CodeTranslator<PartStage> {
        PODate("PODT"), ShippedDate("SHIP"), ApprovedDate("APRV"), FirstApproval("FARV");
        private String code;

        PartStage(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        @Override
        public PartStage translate(String code) {
            for (PartStage s : PartStage.values()) {
                if (StringUtils.equalsIgnoreCase(s.name(), code)) {
                    return s;
                }
            }
            return null;
        }
    }

    public enum PartType implements CodeTranslator<PartType> {
        Tool("TOOL"), FAI("FAI"), Production("PROD");
        private String code;

        PartType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        @Override
        public PartType translate(String code) {
            for (PartType s : PartType.values()) {
                if (StringUtils.equalsIgnoreCase(s.name(), code)) {
                    return s;
                }
            }
            return null;
        }
    }

    public enum PartStatus implements CodeTranslator<PartStatus> {
        Sample("SAMP"), PPAP("PPAP"), Production("PROD");
        private String code;

        PartStatus(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        @Override
        public PartStatus translate(String code) {
            for (PartStatus s : PartStatus.values()) {
                if (StringUtils.equalsIgnoreCase(s.name(), code)) {
                    return s;
                }
            }
            return null;
        }
    }
      public enum PartResource implements CodeTranslator<PartResource> {
        Resourcing("RES"), NPI("NPI");
        private String code;

        PartResource(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        @Override
        public PartResource translate(String code) {
            for (PartResource s : PartResource.values()) {
                if (StringUtils.equalsIgnoreCase(s.name(), code)) {
                    return s;
                }
            }
            return null;
        }
    }
}
