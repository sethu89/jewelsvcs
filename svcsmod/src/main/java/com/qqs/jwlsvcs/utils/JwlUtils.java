package com.qqs.jwlsvcs.utils;

public class JwlUtils {
    public static String extractBarCodeChar(String barCode) {
        if(barCode == null || "".equals(barCode) || barCode.length() < 8 ) return "";
        return  barCode.substring(0,barCode.length()-6);
    }
}
