package com.qqs.jwlsvcs.utils;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImage;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Rama Sudalayandi on 4/02/2017.
 */
@Component
public class PDFCreateService {
    Logger log = LoggerFactory.getLogger(PDFCreateService.class);

    public void fillForm(Map<String, String> params, OutputStream stream) throws IOException {
        getPdfContent(params, stream, "/td_app.pdf");
    }

    public void getPdfContent(Map<String, String> params, OutputStream stream, String pdfSource) throws IOException {
        Resource resource = new ClassPathResource(pdfSource);

        try (InputStream inputStream = resource.getInputStream()) {  // our temp
            PDDocument pdfDocument = PDDocument.load(inputStream);
            for (String key : params.keySet()) {
                setField(key, params.get(key), pdfDocument);
            }
            pdfDocument.getDocumentCatalog().getAcroForm().flatten();
            pdfDocument.save(stream);
            pdfDocument.close();
        }
    }


    // For local testing
    public void fillFormLocal(Map<String, String> params) throws IOException {
        fillFormLocal(params, "src/main/resources/InvWithEditField.pdf", "src/main/resources/filled.pdf");
    }

    public void fillFormLocal(Map<String, String> params, String fileSource, String fileDestination) throws IOException {
        try {
            Resource resource = new ClassPathResource(fileSource);

            File initialFile = resource.getFile();

            InputStream targetStream = new FileInputStream(initialFile);
            PDDocument pdfDocument = PDDocument.load(targetStream);
            for (String key : params.keySet()) {
                setField(key, params.get(key), pdfDocument);
            }
            PDAcroForm pDAcroForm = pdfDocument.getDocumentCatalog().getAcroForm();
            pDAcroForm.flatten();
            pdfDocument.save(fileDestination);
            pdfDocument.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void showFields() throws IOException {
        try (InputStream resource = getClass().getResourceAsStream("/td_app.pdf")) {
            PDDocument pdfDocument = PDDocument.load(resource);
            PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();
            List<PDField> fields = acroForm.getFields();
            for (PDField field : fields) {
                list(field);
            }
        }
    }

    private void list(PDField field) {
        System.out.println(field.getFullyQualifiedName());
        System.out.println(field.getPartialName());
        if (field instanceof PDNonTerminalField) {
            PDNonTerminalField nonTerminalField = (PDNonTerminalField) field;
            for (PDField child : nonTerminalField.getChildren()) {
                list(child);
            }
        }
    }

    public void setFields(Map<String, String> params, PDDocument pdfDocument) {
        PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();

        acroForm.getFields().forEach(item -> setField(item.getFullyQualifiedName(), params.get(item.getFieldType()), pdfDocument));

    }


    public void setField(String name, String value, PDDocument pdfDocument) {
        try {
            PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();
            PDField field = acroForm.getField(name);
            if (field != null) {
                if (field instanceof PDCheckBox) {
                    setCheckBox(field, value);
                } else if (field instanceof PDButton) {
                    setButton(field, value);
                } else if (field instanceof PDComboBox) {
                    setComboBox(field, value);
                } else if (field instanceof PDPushButton) {
//                    setImage(field, value);
//                    PDPushButton pdPushButton = (PDPushButton) field;
//                    List<PDAnnotationWidget> widgets = pdPushButton.getWidgets();
//                    if (widgets != null && widgets.size() > 0) {
//                        PDAnnotationWidget annotationWidget = widgets.get(0); // just need one widget
//
//                        String filePath = value;
//                        File imageFile = new File(filePath);
//
//
//                        if (imageFile.exists()) {
//                            /*
//                             * BufferedImage bufferedImage = ImageIO.read(imageFile);
//                             * PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, bufferedImage);
//                             */
//                            PDImageXObject pdImageXObject = PDImageXObject.createFromFile(filePath, pdfDocument);
//                            float imageScaleRatio = (float) pdImageXObject.getHeight() / (float) pdImageXObject.getWidth();
//
//                            PDRectangle buttonPosition = getFieldArea(pdPushButton);
//                            float height = buttonPosition.getHeight();
//                            float width = height / imageScaleRatio;
//                            float x = buttonPosition.getLowerLeftX();
//                            float y = buttonPosition.getLowerLeftY();
//
//                            PDAppearanceStream pdAppearanceStream = new PDAppearanceStream(pdfDocument);
//                            pdAppearanceStream.setResources(new PDResources());
//                            try (PDPageContentStream pdPageContentStream = new PDPageContentStream(pdfDocument, pdAppearanceStream)) {
//                                pdPageContentStream.drawImage(pdImageXObject, x, y, width, height);
//                            }
//
//                            pdAppearanceStream.setBBox(new PDRectangle(x, y, width, height));
//
//                            PDAppearanceDictionary pdAppearanceDictionary = annotationWidget.getAppearance();
//                            if (pdAppearanceDictionary == null) {
//                                pdAppearanceDictionary = new PDAppearanceDictionary();
//                                annotationWidget.setAppearance(pdAppearanceDictionary);
//                            }
//                            pdAppearanceDictionary.setNormalAppearance(pdAppearanceStream);
//                            System.out.println("Image '" + filePath + "' inserted");
//                        } else {
//                            System.err.println("File " + filePath + " not found");
//                        }
//                    } else {
//                        System.err.println("Missconfiguration of placeholder: '" + name + "' - no widgets(actions) found");
//                    }

                } else {
                    field.setValue(value);
                }
                field.setReadOnly(true);
            } else {
                log.error("No field found with name:" + name);
            }
        } catch (Exception e) {
            log.error("Error setting field", e);
        }
    }

    private PDRectangle getFieldArea(PDField field) {
        COSDictionary fieldDict = field.getCOSObject();
        COSArray fieldAreaArray = (COSArray) fieldDict.getDictionaryObject(COSName.RECT);
        return new PDRectangle(fieldAreaArray);
    }

    private void setButton(PDField field, String value) throws IOException {
        PDButton button = (PDButton) field;
        if (validBoolean(value)) {
            if (button.isRadioButton()) {
                button.setRadioButton(Boolean.valueOf(value));
            } else if (button.isPushButton()) {
                button.setPushButton(Boolean.valueOf(value));
            }
        } else {
            button.setValue(value);
        }
    }

    private void setCheckBox(PDField field, String value) throws IOException {
        PDCheckBox checkBox = (PDCheckBox) field;
        if (validBoolean(value)) {
            if (Boolean.valueOf(value)) {
                checkBox.check();
            } else {
                checkBox.unCheck();
            }
        } else {
            checkBox.setValue(value);
        }

    }

    private void setComboBox(PDField field, String value) throws IOException {
        PDComboBox comboBox = (PDComboBox) field;
        if (value.contains(",")) {
            comboBox.setValue(Arrays.asList(value));
        } else {
            comboBox.setValue(value);
        }
    }

    private void setImage(PDField field, String value) throws IOException {
        File fileToBeReaded = new File(value);

        PDImage image = (PDImage) field;
//        if (value.contains(",")) {
        image.getImage();
//            image.createInputStream(fileToBeReaded);
//            createPDFFromImage();
//            image.
//        } else {
//            image.createInputStream();
//        }
    }

    private boolean validBoolean(String value) {
        if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
            return true;
        }
        return false;
    }

    public void createPDFFromImage(String inputFile, String imagePath, String outputFile)
            throws IOException {
        try (PDDocument doc = PDDocument.load(new File(inputFile))) {
            //we will add the image to the first page.
            PDPage page = doc.getPage(0);

            // createFromFile is the easiest way with an image file
            // if you already have the image in a BufferedImage,
            // call LosslessFactory.createFromImage() instead
            PDImageXObject pdImage = PDImageXObject.createFromFile(imagePath, doc);

            try (PDPageContentStream contentStream = new PDPageContentStream(doc, page, PDPageContentStream.AppendMode.APPEND, true, true)) {
                // contentStream.drawImage(ximage, 20, 20 );
                // better method inspired by http://stackoverflow.com/a/22318681/535646
                // reduce this value if the image is too large
                float scale = 1f;
                contentStream.drawImage(pdImage, 20, 20, pdImage.getWidth() * scale, pdImage.getHeight() * scale);
            }
            doc.save(outputFile);
        }
    }
}
