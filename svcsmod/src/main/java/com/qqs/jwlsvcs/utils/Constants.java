package com.qqs.jwlsvcs.utils;

import com.google.common.collect.ImmutableMap;

import java.util.*;

public final class Constants {

    private Constants() {
        // restrict instantiation
    }

    public static final int VENDOR_ID = 1;
    public static final String INVOICE_TITLE = "EXPORT INVOICE";


    public static final Map<String, String> uploadFileFolder = new HashMap<>();
    static {
        uploadFileFolder.put("standard", "standards");
        uploadFileFolder.put("productImage", "productImage");
        uploadFileFolder.put("atsContentSheet", "atsContentSheet");
        uploadFileFolder.put("PkgCheckList", "PkgCheckList");
        uploadFileFolder.put("purchaseOrder", "purchaseOrder");
        uploadFileFolder.put("awbInvoice", "awbInvoice");
    }

    public static final String PRODUCT_STATUS_INACTIVE = "INACTIVE";


    public static final Map<String, String> INVOICE_STATUS_COMPLETE = new HashMap<>();
    static {
        INVOICE_STATUS_COMPLETE.put("AWBBLATTR2", "Yes");
        INVOICE_STATUS_COMPLETE.put("INVCUATTR2", "Sent");
        INVOICE_STATUS_COMPLETE.put("PRATTR3", "Received");
        INVOICE_STATUS_COMPLETE.put("IGRCATTR3", "Received");
        INVOICE_STATUS_COMPLETE.put("DDBATTR3", "Received");
        INVOICE_STATUS_COMPLETE.put("SBRATTR4", "Yes");
        INVOICE_STATUS_COMPLETE.put("FIRCATTR3", "Received");
        INVOICE_STATUS_COMPLETE.put("EDPMSATTR1", "Updated");
        INVOICE_STATUS_COMPLETE.put("MEISATTR2", "Received");
    }

    public static final Map<String, String> INV_SERVICE_MAP = ImmutableMap.of("byId", "invPD/search/byProductIdType?",
            "save", "invPD/save");

    public static final Map<String, String> INV_PO_SERVICE_MAP = ImmutableMap.of("byId", "supplierpurchase/search/byId?",
            "save", "invPD/save");

    public static final Map<String, String> PRODUCT_TYPE_MAP = ImmutableMap.of("part", "part", "tool", "tool",
            "inserts", "inserts", "holder", "holder", "product", "product");

    public static final String SCHEDULE_URL = "/schedule/bySchFromDate?schFromDate=";

    public static final String JOB_TYPE_BO = "BO";

}
