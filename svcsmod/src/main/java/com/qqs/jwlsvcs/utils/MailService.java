package com.qqs.jwlsvcs.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.util.Properties;

public class MailService {

    @Value("${app.document.upload.location}")
    private static String uploadFolder;

    Logger log = LoggerFactory.getLogger(PDFCreateService.class);


    public static void sendEmail(ByteArrayOutputStream stream) {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();

        String host = "smtp.qqsworld.com";
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "25"); //25 //465
        props.put("mail.smtp.auth", "true");
        props.put("mail.imaps.partialfetch", "false");

        // sender.setHost("smtp.qqsworld.com");
        sender.setUsername("info@qqsworld.com");
        sender.setPassword("QinfoQ#123");
        sender.setJavaMailProperties(props);

        // sender.setPort(25);
        MimeMessage message = sender.createMimeMessage();

        // use the true flag to indicate you need a multipart message
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo("dhaarun@qqsworld.com");
//            helper.setCc("quality@qqsworld.com");
            helper.setFrom("no-rply@qqsworld.com");
            helper.setText("Check out this file!");
            helper.setSubject("Packing Check List");

            // FileSystemResource file = new FileSystemResource(new File("ATSFORM.pdf"));
            org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());

            helper.addAttachment("PkgChkList.pdf", file);

            sender.send(message);
            System.out.println("message sent successfully...");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
