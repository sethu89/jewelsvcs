package com.qqs.qqsoft.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSWebFilter implements Filter{

    private static String SERVER_PORT = ":4200";
    private static String PROTOCOL = "http://";
    @Value("${com.qq.servermode:false}")
    private boolean serverMode = false;
    @Value("${com.qq.server_origin_url:http://ec2-18-222-206-235.us-east-2.compute.amazonaws.com:9080}")
    private String serverOriginURL;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if(!serverMode) {
            String origin = request.getHeader("origin");
            //response.setHeader("Access-Control-Allow-Origin", "http://" + "192.168.1.14" + ":9080");
            //response.setHeader("Access-Control-Allow-Origin", PROTOCOL + origin + SERVER_PORT);
            response.setHeader("Access-Control-Allow-Origin", origin);
        } else {
            response.setHeader("Access-Control-Allow-Origin", serverOriginURL);
        }
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with, accept, authorization, content-type, X-QQ-Auth-token");
        response.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Credentials, X-QQ-Auth-token, X-QQ-Auth-roles");
        //response.addHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        //send roles back to client
        //response.setHeader("X-QQ-Auth-roles", getUserRoles());
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    private String getUserRoles(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        StringBuffer buffer = new StringBuffer();
        for(GrantedAuthority authority : user.getAuthorities()){
            buffer.append(StringUtils.removeStart(authority.getAuthority(), "ROLE_"));
            buffer.append('|');
        }
        return buffer.toString();
    }
}
