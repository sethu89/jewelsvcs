package com.qqs.qqsoft.config;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

/**
 * Web services support configuration
 * <p>
 * Use the Poolable weservice templates for production ready ws calls
 * The Poolable templates use Apache pooling HttpClients
 */

@Configuration
public class WebServiceConfiguration {
    @Value("${app.http.connectionTimeout:60000}")
    private int connectionTimeout;
    @Value("${app.http.connectionRequestTimeout:60000}")
    private int connectionRequestTimeout;
    @Value("${app.http.socketTimeout:1000}")
    private int socketTimeout;
    @Value("${app.http.connectionManagerMaxTotal:60000}")
    private int connectionManagerMaxTotal;
    @Value("${app.http.idleEvictionTimeout:300000}")
    private int idleEvictionTimeout;
    @Value("${app.http.connectionManagerMaxPerRoute:10000}")
    private int connectionManagerMaxPerRoute;

    @Bean(name = "poolableRestTemplate")
    public RestTemplate restTemplate(HttpClient httpClient) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(factory);
        return restTemplate;
    }

    @Bean(name = "poolableHttpClient")
    public HttpClient getHttpClient() {
        /* Use for SSL specific cert enabling
        try {
            KeyStore myTrustStore = KeyStore.getInstance("JKS");
            FileInputStream keyStoreStream = null;
            String keyPassword = "test";
            myTrustStore.load(keyStoreStream, keyPassword.toCharArray());
            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(myTrustStore)
                    .build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);
        } catch (Exception e){
            // Do something...
        }
        */
        ConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
        LayeredConnectionSocketFactory sslsf = SSLConnectionSocketFactory.getSocketFactory();
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", plainsf)
                .register("https", sslsf)
                .build();

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
        cm.setMaxTotal(connectionManagerMaxTotal);
        cm.setDefaultMaxPerRoute(connectionManagerMaxPerRoute);
        // If the connection is for specific route
        //HttpHost localhost = new HttpHost("locahost", 80);
        //cm.setMaxPerRoute(new HttpRoute(localhost), 50);

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(connectionTimeout)
                .setConnectionRequestTimeout(connectionRequestTimeout)
                .setSocketTimeout(socketTimeout).build();

        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(cm)
                .setDefaultRequestConfig(config)
                .evictExpiredConnections()
                .evictIdleConnections(idleEvictionTimeout, TimeUnit.MILLISECONDS)
                .build();
        return httpClient;
    }
}
