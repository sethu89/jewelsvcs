package com.qqs.qqsoft.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DateFormatUtils {
    public static List<String> getMonthsBetween(String fromDate, String toDate) {
        List<String> returnVal = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outPutFormat = new SimpleDateFormat("yyyy-MM");

        Calendar beginCalendar = Calendar.getInstance();
        Calendar finishCalendar = Calendar.getInstance();

        try {
            beginCalendar.setTime(dateFormat.parse(fromDate));
            finishCalendar.setTime(dateFormat.parse(toDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        while (beginCalendar.before(finishCalendar)) {
            // add one month to date per loop
            String date = outPutFormat.format(beginCalendar.getTime()).toUpperCase();
            returnVal.add(date);
            beginCalendar.add(Calendar.MONTH, 1);
        }
        return returnVal;
    }
}
