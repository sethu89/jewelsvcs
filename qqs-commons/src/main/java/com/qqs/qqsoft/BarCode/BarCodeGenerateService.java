package com.qqs.qqsoft.BarCode;

import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import java.awt.image.BufferedImage;
import java.io.*;


import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.scene.control.Button;
//import javafx.scene.control.Label;
//import javafx.scene.control.TextField;
//import javafx.scene.image.ImageView;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;


public class BarCodeGenerateService {

//    private String barCodePath = "../../../Documents/";
    private String barCodePath = "/Users/dhaarunraja/Documents/";


//    public void createBarCode128(String fileName) {
//        try {
//            Code128Bean bean = new Code128Bean();
//            final int dpi = 160;
//
//            //Configure the barcode generator
//            bean.setModuleWidth(UnitConv.in2mm(2.8f / dpi));
//
//            bean.doQuietZone(false);
//
//            //Open output file
//            File outputFile = new File(barCodePath + fileName + ".JPG");
//
//            FileOutputStream out = new FileOutputStream(outputFile);
//
//            BitmapCanvasProvider canvas = new BitmapCanvasProvider(
//                    out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
//
//            //Generate the barcode
//            bean.generateBarcode(canvas, fileName);
//
//            //Signal end of generation
//            canvas.finish();
//
//            System.out.println("Bar Code is generated successfully…");
//        }
//        catch (IOException ex) {
//            ex.printStackTrace();
//        }
//    }





    public void createBarCode39(String fileName) {

        try {
            Code39Bean bean39 = new Code39Bean();
            final int dpi = 160;

            //Configure the barcode generator
            bean39.setModuleWidth(UnitConv.in2mm(2.8f / dpi));

            bean39.doQuietZone(false);

            //Open output file
            File outputFile = new File(barCodePath + fileName + ".JPG");
            System.out.println("outputFile");
            System.out.println(barCodePath + fileName + ".JPG");

            FileOutputStream out = new FileOutputStream(outputFile);


            //Set up the canvas provider for monochrome PNG output
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                    out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);

            //Generate the barcode
            bean39.generateBarcode(canvas, fileName);

            //Signal end of generation
            canvas.finish();

            System.out.println("Bar Code is generated successfully…");
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void generateBarcode(String serialNo, Integer fromNumber, Integer noOfLables,  ByteArrayOutputStream stream)
    {
        BarCodeGenerateService barCodeGenerateServiceUtil = new BarCodeGenerateService();
        // This will generate Bar-Code 3 of 9 format
        try {
            barCodeGenerateServiceUtil.code(serialNo, fromNumber, noOfLables, stream);
        } catch (Exception e) {
            System.out.println("Newly added error");
        }
    }



    public void code(String serialNo, Integer fromNumber, Integer noOfLables, ByteArrayOutputStream stream) throws FileNotFoundException, IOException, BadElementException, DocumentException {
        Code128Bean code128 = new Code128Bean();
        code128.setHeight(15.0D);
        code128.setModuleWidth(0.3D);
        code128.setQuietZone(10.0D);
        code128.doQuietZone(true);
        Document document = new Document();
        PdfPTable table = new PdfPTable(3);
        table.getDefaultCell().setBorder(0);

        var lastNumber  = fromNumber + noOfLables;
        for(int aw = fromNumber; aw < lastNumber; ++aw) {
            PdfPTable intable = new PdfPTable(1);
            intable.getDefaultCell().setBorder(0);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 400, 12, false, 0);
            String digit = "";
                digit = String.format("%06d", aw);

            code128.generateBarcode(canvas, serialNo + digit);
            System.out.println(serialNo + aw);
            canvas.finish();
            FileOutputStream fos = new FileOutputStream("barcode.png");
            fos.write(baos.toByteArray());
            fos.flush();
            fos.close();
            Image png = Image.getInstance(baos.toByteArray());
            png.setAbsolutePosition(0.0F, 705.0F);
            png.scalePercent(25.0F);
            intable.addCell(png);
            intable.getDefaultCell().setBorder(0);
            table.addCell(intable);
        }
        table.completeRow();
//        ByteArrayOutputStream out = new ByteArrayOutputStream();

        PdfWriter writer = PdfWriter.getInstance(document, stream);
        document.open();
        document.add(table);
        document.close();
        writer.close();
//        return new ByteArrayInputStream(out.toByteArray());


    }

//    public InputStream createPdf() throws IOException {
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        try (Document doc = new Document(PageSize.A4, 50, 50, 50, 50)) {
//            PdfWriter writer = PdfWriter.getInstance(doc, out);
//            doc.open();
//            PdfPTable table = new PdfPTable(1);
//            PdfPCell cell = new PdfPCell(new Phrase("First PDF"));
//            cell.setBorder(Rectangle.NO_BORDER);
//            cell.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
//            table.addCell(cell);
//            doc.add(table);
//        }
//        return new ByteArrayInputStream(out.toByteArray());
//    }
}
