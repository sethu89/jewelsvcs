package com.qqs.qqsoft.mail;

import com.qqs.qqsoft.pdf.PDFCreateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.util.Properties;

public class MailService {

    @Value("${app.document.upload.location}")
    private static String uploadFolder;

    Logger log = LoggerFactory.getLogger(PDFCreateService.class);


    public static void sendEmail(ByteArrayOutputStream stream) {
        JavaMailSenderImpl sender = propsFile();
        MimeMessage message = sender.createMimeMessage();

        // use the true flag to indicate you need a multipart message
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo("dhaarun@qqsworld.com");
            helper.setFrom("no-rply@qqsworld.com");
            helper.setText("Check out this file!");
            helper.setSubject("Packing Check List");

            org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());

            helper.addAttachment("PkgChkList.pdf", file);

            sender.send(message);
            System.out.println("message sent successfully...");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public static void sendPOByEmail(ByteArrayOutputStream stream) {
        JavaMailSenderImpl sender = propsFile();

        MimeMessage message = sender.createMimeMessage();

        // use the true flag to indicate you need a multipart message
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo("dhaarun@qqsworld.com");
            helper.setFrom("no-rply@qqsworld.com");
            helper.setText("PFA of the PO Sample Test! This is not valid PO. ONly for automation testing");
            helper.setText("<html> <body><h1>Hello /n </h1> </body></html> " +
                    "<html> <body><h4>Product :</h4>  </body></html>  " +

                    " <input type=submit class=btn btn-success customBtn id=submit value=Submit>", true);
            helper.setSubject("PO Sample Test");

            org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());

            helper.addAttachment("POFileTestSample.pdf", file);
            sender.send(message);
            System.out.println("message sent successfully...");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

//    public static void poForApproval(SupplierPurchaseOrder po) {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        JavaMailSenderImpl sender = propsFile();
//        MimeMessage message = sender.createMimeMessage();
//        // use the true flag to indicate you need a multipart message
//        MimeMessageHelper helper = null;
//        try {
//            helper = new MimeMessageHelper(message, true);
//            helper.setTo("dhaarun@qqsworld.com");
//            helper.setFrom("no-rply@qqsworld.com");
//            StringBuffer sb = new StringBuffer();
//            sb.append("PoNumber: " + po.getPoNumber());
//            sb.append("PO Date: " + po.getPoDate());
//            sb.append("Supplier Name: " + po.getSupplier().getName());
//            sb.append("Supplier City: " + po.getSupplier().getAddresses().get(0).getCity());
//            po.getSupplierPoLineItem().forEach(supplierPoLineItem -> {
//                sb.append("ProductName: " + supplierPoLineItem.getSupplierXProduct().getProductName());
//                sb.append("QTY: " + supplierPoLineItem.getQuantity());
//                sb.append("Price: " + supplierPoLineItem.getCost());
//            });
//            helper.setText(sb.toString());
////            helper.setText("PFA of the PO Sample Test! This is not valid PO. ONly for automation testing");
////            helper.setText("<html> <body><h1>Hello /n </h1> </body></html> " +
////                    "<html> <body><h4>Product :</h4> " + po.getPoStatus() + "</body></html>  " +
////
////                    " <input type=submit class=btn btn-success customBtn id=submit value=Submit>", true);
//            helper.setSubject("PO Sample Test");
//            org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());
//            helper.addAttachment("POFileTestSample.pdf", file);
//            sender.send(message);
//            System.out.println("message sent successfully...");
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
//    }

//    public static void pendingOrderMail(List<PendingOrderData> pendingOrderList) {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        JavaMailSenderImpl sender = propsFile();
//        MimeMessage message = sender.createMimeMessage();
//        // use the true flag to indicate you need a multipart message
//        MimeMessageHelper helper = null;
//        try {
//            helper = new MimeMessageHelper(message, true);
//            helper.setTo("dhaarun@qqsworld.com");
//            helper.setFrom("no-rply@qqsworld.com");
//            StringBuffer sb = new StringBuffer();
//
//            // Mail Table Block Code.
//
//            sb.append("<html><body>"
//                    + "<table style='border:2px solid black'>");
//
//            System.out.println("in results...");
//            sb.append("<tr bgcolor=\"#33CC99\">");
//            sb.append(" <thead>\n" +
//                    "            <tr bgcolor=\"#33CC99\">\n" +
//                    "              <th>Company Name</th>\n" +
//                    "              <th>Plant Name</th>\n" +
//                    "              <th>Po Number</th>\n" +
//                    "              <th>Part Number</th>\n" +
//                    "              <th>Part Rev</th>\n" +
//                    "              <th>Days Left</th>\n" +
//                    "              <th>Delivery Qty</th>\n" +
//                    "              <th>Pending Qty</th>\n" +
//                    "            </tr>\n" +
//                    "          </thead>");
//            pendingOrderList.forEach(pendingOrder -> {
//                sb.append("<tr>");
//                sb.append("<td>");
//                sb.append(pendingOrder.getCompanyName().toString());
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(pendingOrder.getPlantName().toString());
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(pendingOrder.getPoNumber());
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(pendingOrder.getPartNumber());
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(pendingOrder.getPartRevNo());
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(pendingOrder.getDaysLeft());
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(Math.round(pendingOrder.getDeliveredQty()));
//                sb.append("</td>");
//
//                sb.append("<td>");
//                sb.append(Math.round(pendingOrder.getPendingQty()));
//                sb.append("</td>");
//
//                sb.append("</tr>");
//            });
//
//            sb.append("<tr> </table></body></html>");
//
//            helper.setText(sb.toString(), true);
//
//            helper.setSubject("Pending Order");
//
//            org.springframework.core.io.Resource file = new ByteArrayResource(stream.toByteArray());
////            helper.addAttachment("POFileTestSample.pdf", file);
//            sender.send(message);
//            System.out.println("message sent successfully...");
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
//    }

    public static JavaMailSenderImpl propsFile() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();


        String host = "smtp.qqsworld.com";

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "25"); //25 //465
        props.put("mail.smtp.auth", "true");
        props.put("mail.imaps.partialfetch", "false");

        // sender.setHost("smtp.qqsworld.com");
        sender.setUsername("info@qqsworld.com");
        sender.setPassword("QinfoQ#123");
        sender.setJavaMailProperties(props);


        return sender;
    }
}
