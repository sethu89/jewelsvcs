USE `jwlmgmnt`;


CREATE TABLE `jwlmgmnt`.`codeassociation` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(24) NOT NULL,
  `picklist` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `codes` */


CREATE TABLE `jwlmgmnt`.`codes` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(24) NOT NULL,
  `code` varchar(24) NOT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `activeDt` datetime DEFAULT NULL,
  `expireDt` datetime DEFAULT NULL,
  `notes` varchar(512) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `jwlmgmnt`.`employee` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `empId` varchar(20) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `jwlmgmnt`.`bank` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `bankName` varchar(500) DEFAULT NULL,
  `accountNumber` varchar(50) DEFAULT NULL,
  `ifscCode` varchar(50) DEFAULT NULL,
  `swiftcode` varchar(45) DEFAULT NULL,
  `adCode` varchar(45) DEFAULT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `jwlmgmnt`.`codes`
ADD COLUMN `isUserGenerated` VARCHAR(1) NULL DEFAULT NULL AFTER `description`,
CHANGE COLUMN `desc``description` VARCHAR(1000) NULL DEFAULT NULL,
ADD COLUMN `displaySeq` INT(10) NULL DEFAULT NULL AFTER `notes`,
ADD COLUMN `createdBy` INT(10) NULL DEFAULT NULL AFTER `notes`,
ADD COLUMN `modifiedBy` INT(10) NULL DEFAULT NULL AFTER `createdDt`;

ALTER TABLE `jwlmgmnt`.`codeassociation`
ADD COLUMN `createdBy` INT(10) NULL DEFAULT NULL AFTER `picklist`,
ADD COLUMN `createdDt` DATETIME NULL DEFAULT NULL AFTER `createdBy`,
ADD COLUMN `modifiedBy` INT(10) NULL DEFAULT NULL AFTER `createdDt`,
ADD COLUMN `modifiedDt` DATETIME NULL DEFAULT NULL AFTER `modifiedBy`;


ALTER TABLE `jwlmgmnt`.`codes`
ADD COLUMN `createdBy` INT(10) NULL DEFAULT NULL AFTER `notes`,
ADD COLUMN `modifiedBy` INT(10) NULL DEFAULT NULL AFTER `createdDt`;

CREATE TABLE `jwlmgmnt`.`user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(25) DEFAULT NULL,
  `lastName` varchar(25) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `pwd` varchar(256) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `roles` varchar(256) DEFAULT NULL,
  `createdDt` date DEFAULT NULL,
  `modifiedDt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

ALTER TABLE `jwlmgmnt`.`user`
ADD COLUMN  `createdBy` int(20) DEFAULT NULL after `roles`,
ADD COLUMN  `modifiedBy` int(20) DEFAULT NULL after `createdDt`;

CREATE TABLE `jwlmgmnt`.`productmaster` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `imagePath` varchar(100) DEFAULT NULL,
  `barCodeChar` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `jwlmgmnt`.`propattributes` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
    `productMasterId` varchar(10) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `jwlmgmnt`.`propattributes`
ADD COLUMN  `label` varchar(100) DEFAULT NULL after `productMasterId`;


INSERT INTO `jwlmgmnt`.`codeassociation` (`id`, `category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('1', 'ACTIVE_STATUS', 'Y', '0', '2020-01-07 09:22:00', '0', '2020-01-07 09:21:51');


INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('1', 'ACTIVE_STATUS', 'true', 'Active', '2018-12-27 18:29:00', '2028-12-27 18:29:00', '', '1', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('2', 'ACTIVE_STATUS', 'false', 'Inactive', '2018-12-27 18:29:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');

INSERT INTO `jwlmgmnt`.`codeassociation` (`id`, `category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('2', 'PRODUCT_WT_UNIT', 'Y', '0', '2020-01-07 09:22:00', '0', '2020-01-07 09:21:51');

INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('3', 'PRODUCT_WT_UNIT', 'LBS', 'lbs', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('4', 'PRODUCT_WT_UNIT', 'KGS', 'Kilogram', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('5', 'PRODUCT_WT_UNIT', 'SET', 'Set', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('6', 'PRODUCT_WT_UNIT', 'GM', 'Grams', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');


CREATE TABLE `jwlmgmnt`.`desk` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `status` char(10) DEFAULT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



CREATE TABLE `jwlmgmnt`.`jobtransaction` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `userId` bigint(10) NOT NULL,
  `productId` bigint(10) NOT NULL,
  `deskId` bigint(10) NOT NULL,
  `inTime` DateTime NOT NULL,
  `outTime` DateTime,
  `jobType` varchar(100) DEFAULT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
)


ALTER TABLE `jwlmgmnt`.`propattributes`
CHANGE COLUMN `productMasterId` `productMasterId` int(20) NULL DEFAULT NULL;


CREATE TABLE `jwlmgmnt`.`product` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `productMasterId` bigint(10) NOT NULL,
  `barCode` varchar(20) NOT NULL,
  `productStatus` varchar(20) NOT NULL,
  `processStatus` varchar(20) NOT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
)

ALTER TABLE `jwlmgmnt`.`product`
ADD UNIQUE INDEX `barCode_UNIQUE` (`barCode` ASC) ;


ALTER TABLE `jwlmgmnt`.`jobtransaction`
ADD COLUMN `status` VARCHAR(20) NULL DEFAULT NULL AFTER `jobType`;

INSERT INTO `jwlmgmnt`.`codeassociation` (`id`, `category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('6', 'DESIGNATION', 'Y', '0', '2020-01-07 09:22:00', '0', '2020-01-07 09:21:51');
INSERT INTO `jwlmgmnt`.`codeassociation` (`id`, `category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('7', 'ROLES', 'Y', '0', '2020-01-07 09:22:00', '0', '2020-01-07 09:21:51');
ALTER TABLE `jwlmgmnt`.`jobtransaction`
CHANGE COLUMN `deskId` `deskId` BIGINT(10) NULL ;

ALTER TABLE `jwlmgmnt`.`jobtransaction`
ADD COLUMN `remarks` VARCHAR(1000) NULL AFTER `jobType`;



INSERT INTO `jwlmgmnt`.`codeassociation` (`id`, `category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('3', 'PRODUCT_LABEL', 'Y', '0', '2020-01-07 09:22:00', '0', '2020-01-07 09:21:51');

INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('7', 'PRODUCT_LABEL', 'PLSN', 'Style Number', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('8', 'PRODUCT_LABEL', 'PLMT', 'Metal Type', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('9', 'PRODUCT_LABEL', 'PLMC', 'Metal Color', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('10', 'PRODUCT_LABEL', 'PLCS', 'Center Stone', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('11', 'PRODUCT_LABEL', 'PLCT', 'Center Stone Type', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('12', 'PRODUCT_LABEL', 'PLSS', 'Side Stone', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('13', 'PRODUCT_LABEL', 'PLST', 'Side Stone Type', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('14', 'PRODUCT_LABEL', 'PLNS', 'Number of Side Stone', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('15', 'PRODUCT_LABEL', 'PLICW', 'Initial Casting Weight', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`id`, `category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('16', 'PRODUCT_LABEL', 'PLFCW', 'Final Casting Weight', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');

-- --------11-July-2020-----------
ALTER TABLE `jwlmgmnt`.`user`
ADD COLUMN `deskId` BIGINT(10) NULL AFTER `roles`;

INSERT INTO `jwlmgmnt`.`codeassociation` (`category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`) VALUES ('ROLES', 'Y', '0', '2020-07-11', '0');

INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `isUserGenerated`, `activeDt`, `expireDt`, `createdBy`, `displaySeq`, `createdDt`, `modifiedBy`) VALUES ('ROLES', 'ADM', 'ADMIN', 'N', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '1', '2020-07-11', '0');
INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `isUserGenerated`, `activeDt`, `expireDt`, `createdBy`, `displaySeq`, `createdDt`, `modifiedBy`) VALUES ('ROLES', 'OPR', 'OPERATOR', 'N', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '1', '2020-07-11', '0');
INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `isUserGenerated`, `activeDt`, `expireDt`, `createdBy`, `displaySeq`, `createdDt`, `modifiedBy`) VALUES ('ROLES', 'MNG', 'MANAGER', 'N', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '1', '2020-07-11', '0');

ALTER TABLE `jwlmgmnt`.`jobtransaction`
ADD COLUMN `status` VARCHAR(45) NOT NULL DEFAULT 'Approved' AFTER `remarks`;

ALTER TABLE `jwlmgmnt`.`product`
ADD INDEX `productMasterId_INDEX` (`productMasterId` ASC);


CREATE TABLE `jwlmgmnt`.`barcodedetails` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `productMasterId` bigint(10) NOT NULL,
  `generatedDate` Date DEFAULT NULL,
  `startCode` VARCHAR(45) NOT NULL,
  `endCode` VARCHAR(45) NOT NULL,
  `fromNumber` INT(20) NOT NULL,
  `noOfLables` INT(20) NOT NULL,
  `createdBy` int(20) DEFAULT NULL,
  `createdDt` datetime DEFAULT NULL,
  `modifiedBy` int(20) DEFAULT NULL,
  `modifiedDt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
)

ALTER TABLE `jwlmgmnt`.`desk`
ADD COLUMN `deskType` VARCHAR(10) NULL AFTER `description`;

INSERT INTO `jwlmgmnt`.`codeassociation` (`category`, `picklist`, `createdBy`, `createdDt`) VALUES ('DESK_TYPES', 'Y', '0', '2020-07-13');

INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `isUserGenerated`, `activeDt`, `expireDt`, `createdBy`, `displaySeq`, `createdDt`, `modifiedBy`) VALUES ('DESK_TYPES', 'STR', 'SETTER', 'Y', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '1', '2020-07-11', '0');
INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `isUserGenerated`, `activeDt`, `expireDt`, `createdBy`, `displaySeq`, `createdDt`, `modifiedBy`) VALUES ('DESK_TYPES', 'GSM', 'GOLD SMITH', 'Y', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '1', '2020-07-11', '0');
INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `isUserGenerated`, `activeDt`, `expireDt`, `createdBy`, `displaySeq`, `createdDt`, `modifiedBy`) VALUES ('DESK_TYPES', 'MGR', 'MANAGER', 'Y', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '0', '1', '2020-07-11', '0');


ALTER TABLE `jwlmgmnt`.`product`
ADD COLUMN `finalStatus` VARCHAR(45) NULL DEFAULT NULL AFTER `processStatus`,
ADD COLUMN `finalStatusUserId` INT(20) NULL DEFAULT NULL AFTER `finalStatus`,
ADD COLUMN `finalStatusDt` DATETIME NULL DEFAULT NULL AFTER `finalStatusUserId`,
ADD COLUMN `customerName` VARCHAR(100) NULL DEFAULT NULL AFTER `finalStatusDt`,
ADD COLUMN `remarks` VARCHAR(500) NULL DEFAULT NULL AFTER `customerName`;



INSERT INTO `jwlmgmnt`.`codeassociation` (`category`, `picklist`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`)
 VALUES ('FINAL_STATUS', 'Y', '0', '2020-01-07 09:22:00', '0', '2020-01-07 09:21:51');

INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ('FINAL_STATUS', 'SLD', 'Sold', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ( 'FINAL_STATUS', 'BRK', 'Broken', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
INSERT INTO `jwlmgmnt`.`codes` (`category`, `code`, `description`, `activeDt`, `expireDt`, `notes`, `displaySeq`, `createdBy`, `createdDt`, `modifiedBy`, `modifiedDt`) VALUES ( 'FINAL_STATUS', 'STK', 'Stock', '2018-12-01 00:00:00', '2028-12-27 18:29:00', '', '0', '0', '2020-01-07 09:20:24', '0', '2020-01-07 09:20:34');
