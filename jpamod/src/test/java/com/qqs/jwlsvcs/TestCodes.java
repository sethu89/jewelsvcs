package com.qqs.jwlsvcs;

import com.qqs.jwlsvcs.model.Codes;
import com.qqs.jwlsvcs.service.CodesDataService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Optional;

public class TestCodes {
    static ApplicationContext context;

    @BeforeClass
    public static void setup() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @Test
    public void testRetrieveByCategory() {
        CodesDataService dataService = context.getBean(CodesDataService.class);
        Optional<List<Codes>> codes = dataService.getCodesForCategory("PART_CATEGORY");
        System.out.println(codes.get());
    }
}
