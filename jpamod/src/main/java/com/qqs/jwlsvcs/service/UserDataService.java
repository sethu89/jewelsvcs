package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.UserEntity;
import com.qqs.jwlsvcs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class UserDataService {
    @Autowired
    private UserRepository repository;

    public Optional<UserEntity> getMassageById(Integer id) {
        return repository.findById(id);
    }

    public Optional<List<UserEntity>> getUserByName(String firstName, String lastName) {
        return repository.findUserEntitiesByName(firstName, lastName);
    }

    public Optional<UserEntity> getUserByUserName(String userName) {
        return repository.findUserEntitiesByUserName(userName);
    }

    public Optional<UserEntity> getUserById(Integer id) {
        return repository.findById(id);
    }


    @Transactional
    public UserEntity saveUser(UserEntity item) {
        return repository.save(item);
    }
}
