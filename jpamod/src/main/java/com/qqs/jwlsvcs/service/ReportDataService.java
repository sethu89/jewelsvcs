package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.*;
import com.qqs.jwlsvcs.repository.JobTransactionReportRepository;
import com.qqs.jwlsvcs.repository.JobTransactionRepository;
import com.qqs.qqsoft.QQBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component

public class ReportDataService {

    @Autowired
    private JobTransactionReportRepository jobTransactionReportRepository;

    @Autowired
    private JobTransactionRepository jobTransactionRepository;

    public List<JobTransactionReportData> getJobTransactionReportData(String fromDate, String toDate,
                                                                      Integer productMasterId, String inventoryStatus){
        return jobTransactionReportRepository.getJobTransactionReportData(fromDate, toDate, productMasterId, inventoryStatus);
    }

    public List<JobTransactionData> getJobTransactionData(String fromDate, String toDate, Integer productMasterId){
        return jobTransactionRepository.getJobTransactionData(fromDate, toDate, productMasterId);
    }

    public List<ProductMasterReportData> getProductMasterReportData(String fromDate, String toDate, Integer productMasterId){
        return jobTransactionReportRepository.getProductMasterReportData(fromDate, toDate, productMasterId);
    }

    public List<ProductDetailReportData> getProductDetailReportData(String fromDate, String toDate, Integer productMasterId){
        return jobTransactionReportRepository.getProductDetailReportData(fromDate, toDate, productMasterId);
    }

    public List<DeskReportData> getDeskReportData(String fromDate, String toDate, Integer productMasterId){
        return jobTransactionReportRepository.getDeskReportData(fromDate, toDate, productMasterId);
    }
}
