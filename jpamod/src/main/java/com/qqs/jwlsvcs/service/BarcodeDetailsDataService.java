package com.qqs.jwlsvcs.service;


import com.qqs.jwlsvcs.model.BarcodeDetails;
import com.qqs.jwlsvcs.repository.BarcodeDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class BarcodeDetailsDataService {
    @Autowired
    private BarcodeDetailsRepository barcodeDetailsRepository;

    @Resource
    private EntityManager entityManager;

    private DataServiceUtils<BarcodeDetails> jobTransUtil = new DataServiceUtils<>();

    public Optional<BarcodeDetails> getBarCodeDetails(Integer id) {
        return barcodeDetailsRepository.findById(id);
    }

    public Optional<List<BarcodeDetails>> getAllByProductMasterId(Integer productId) {
        return barcodeDetailsRepository.findAllByProductMasterId(productId);
    }

    public Optional<String> getLastDetailsByProductMasterId(Integer productId) {
        return barcodeDetailsRepository.searchLastBarcodeByProductMasterId(productId);
    }

    public Optional<List<BarcodeDetails>> searchBarCodeDetails(List<SearchCriteria> params) {
        List<BarcodeDetails> result = jobTransUtil.createPredicate(entityManager, params, BarcodeDetails.class);
        Optional<List<BarcodeDetails>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }


    @Transactional
    public BarcodeDetails saveBarCodeDetails(BarcodeDetails item) {
        return barcodeDetailsRepository.save(item);
    }
}
