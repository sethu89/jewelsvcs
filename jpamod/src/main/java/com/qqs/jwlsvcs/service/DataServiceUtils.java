package com.qqs.jwlsvcs.service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DataServiceUtils<T> {
    public List<T> createPredicate(EntityManager entityManager, List<SearchCriteria> params, Class<T> clazz) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        Predicate predicate = builder.conjunction();
        CriteriaQuery<T> query = builder.createQuery(clazz);
        Root r = query.from(clazz);
        for (SearchCriteria param : params) {
            if(param.getOperation().equalsIgnoreCase("^")) { //between operator
                predicate = builder.and(predicate, buildBetween(builder, r, param));
            } else if (param.getOperation().equalsIgnoreCase(">")) {
                predicate = builder.and(predicate, buildGreaterThan(builder, r, param));
            } else if (param.getOperation().equalsIgnoreCase("<")) {
                predicate = builder.and(predicate, buildLessThan(builder, r, param));
            } else if (param.getOperation().equalsIgnoreCase(":")) {
                if (r.get(param.getKey()).getJavaType() == String.class) {
                    predicate = builder.and(predicate,
                            builder.like(r.get(param.getKey()),
                                    "%" + param.getValue() + "%"));
                } else {
                    predicate = builder.and(predicate,
                            builder.equal(r.get(param.getKey()), convertValue(param, r)));
                }
            } else if (param.getOperation().equalsIgnoreCase("=")) {
                predicate = builder.and(predicate,
                        builder.equal(r.get(param.getKey()), convertValue(param, r)));
            }
        }
        query.where(predicate);

        List<T> result = entityManager.createQuery(query).getResultList();
        return result;
    }

    private Object convertValue(SearchCriteria criteria, Root r) {
        if (r.get(criteria.getKey()).getJavaType() == String.class) {
            return criteria.getValue().toString();
        }
        if (r.get(criteria.getKey()).getJavaType() == Date.class
                || r.get(criteria.getKey()).getJavaType() == java.util.Date.class
                || r.get(criteria.getKey()).getJavaType() == Timestamp.class) {
            LocalDate date;
            if(criteria.getValue().toString().length() > 10) {
                date = LocalDate.parse(criteria.getValue().toString(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            } else {
                date = LocalDate.parse(criteria.getValue().toString(), DateTimeFormatter.ISO_LOCAL_DATE);
            }
            return Date.valueOf(date);
        }
        return criteria.getValue();
    }

    private Object convertValue(SearchCriteria criteria, Root r, Integer index) {
        if(index > 1 || index < 0) return criteria.getValue();
        if (r.get(criteria.getKey()).getJavaType() == String.class) {
            return criteria.getValue().toString().split("\\|\\|")[index];
        }
        if (r.get(criteria.getKey()).getJavaType() == Date.class
                || r.get(criteria.getKey()).getJavaType() == java.util.Date.class
                || r.get(criteria.getKey()).getJavaType() == Timestamp.class) {
            String crValue = criteria.getValue().toString().split("\\|\\|")[index];
            LocalDate date;
            if(crValue.length() > 10) {
                date = LocalDate.parse(crValue, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            } else {
                date = LocalDate.parse(crValue, DateTimeFormatter.ISO_LOCAL_DATE);
            }
            return Date.valueOf(date);
        }
        return criteria.getValue();
    }

    private Predicate buildGreaterThan(CriteriaBuilder builder, Root r, SearchCriteria criteria) {
        if (r.get(criteria.getKey()).getJavaType() == String.class) {
            return builder.greaterThanOrEqualTo(r.<String>get(criteria.getKey()),
                    (String) convertValue(criteria, r));
        }
        if (r.get(criteria.getKey()).getJavaType() == Date.class
                || r.get(criteria.getKey()).getJavaType() == java.util.Date.class
                || r.get(criteria.getKey()).getJavaType() == Timestamp.class) {
            return builder.greaterThanOrEqualTo(r.<Date>get(criteria.getKey()),
                    (java.util.Date) convertValue(criteria, r));
        }
        if (r.get(criteria.getKey()).getJavaType() == Integer.class
                || r.get(criteria.getKey()).getJavaType() == Long.class) {
            return builder.greaterThanOrEqualTo(r.<Long>get(criteria.getKey()),
                    (Long) convertValue(criteria, r));
        }
        return builder.greaterThanOrEqualTo(r.<Long>get(criteria.getKey()),
                0l);
    }

    private Predicate buildBetween(CriteriaBuilder builder, Root r, SearchCriteria criteria) {
        if (r.get(criteria.getKey()).getJavaType() == String.class) {
            return builder.between (r.<String>get(criteria.getKey()),
                    (String) convertValue(criteria, r, 0), (String) convertValue(criteria, r, 1));
        }
        if (r.get(criteria.getKey()).getJavaType() == Date.class
                || r.get(criteria.getKey()).getJavaType() == java.util.Date.class
                || r.get(criteria.getKey()).getJavaType() == Timestamp.class) {
            return builder.between(r.<Date>get(criteria.getKey()),
                    (java.util.Date) convertValue(criteria, r, 0), (java.util.Date) convertValue(criteria, r, 1));
        }
        if (r.get(criteria.getKey()).getJavaType() == Integer.class
                || r.get(criteria.getKey()).getJavaType() == Long.class) {
            return builder.between(r.<Long>get(criteria.getKey()),
                    (Long) convertValue(criteria, r, 0), (Long) convertValue(criteria, r, 1));
        }
        return builder.greaterThanOrEqualTo(r.<Long>get(criteria.getKey()),
                0l);
    }

    private Predicate buildLessThan(CriteriaBuilder builder, Root r, SearchCriteria criteria) {
        if (r.get(criteria.getKey()).getJavaType() == String.class) {
            return builder.lessThanOrEqualTo(r.<String>get(criteria.getKey()),
                    (String) convertValue(criteria, r));
        }
        if (r.get(criteria.getKey()).getJavaType() == Date.class
                || r.get(criteria.getKey()).getJavaType() == java.util.Date.class
                || r.get(criteria.getKey()).getJavaType() == Timestamp.class) {
            return builder.lessThanOrEqualTo(r.<Date>get(criteria.getKey()),
                    (java.util.Date) convertValue(criteria, r));
        }
        if (r.get(criteria.getKey()).getJavaType() == Integer.class
                || r.get(criteria.getKey()).getJavaType() == Long.class) {
            return builder.lessThanOrEqualTo(r.<Long>get(criteria.getKey()),
                    (Long) convertValue(criteria, r));
        }
        return builder.lessThanOrEqualTo(r.<Long>get(criteria.getKey()),
                0l);
    }
}
