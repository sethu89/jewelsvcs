package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.CodeAssociation;
import com.qqs.jwlsvcs.model.Codes;
import com.qqs.jwlsvcs.repository.CodeAssociationRepository;
import com.qqs.jwlsvcs.repository.CodesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

@Component
public class CodesDataService {
    @Resource
    private EntityManager entityManager;

    private DataServiceUtils<CodeAssociation> utils = new DataServiceUtils<>();
    private DataServiceUtils<Codes> codesUtils = new DataServiceUtils<>();

    @Autowired
    private CodesRepository repository;

    @Autowired
    private CodeAssociationRepository associationRepository;

    public Optional<List<Codes>> getCodesForCategory(String category) {
        return repository.findAllByCategory(category);
    }

    public Optional<List<Codes>> getCodesByCode(String code) {
        return repository.findAllByCode(code);
    }

    public Iterable<Codes> getAllCodes() {
        return repository.findAll();
    }

    public Iterable<CodeAssociation> getAllCodeAssociation() {
        return associationRepository.findAll();
    }

    public Optional<CodeAssociation> getCodeAssociationById(Integer id) {
        return associationRepository.findById(id);
    }

    public Optional<List<CodeAssociation>> searchCodeAssoication(List<SearchCriteria> params) {
        List<CodeAssociation> result = utils.createPredicate(entityManager, params, CodeAssociation.class);
        Optional<List<CodeAssociation>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }

    @Transactional
    public CodeAssociation saveCodeAssociation(CodeAssociation item) {
        return associationRepository.save(item);
    }

    public Optional<Codes> getCodesById(Integer id) {
        return repository.findById(id);
    }

    public Optional<List<Codes>> searchCodes(List<SearchCriteria> params) {
        List<Codes> result = codesUtils.createPredicate(entityManager, params, Codes.class);
        Optional<List<Codes>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }

    @Transactional
    public Codes saveCodes(Codes item) {
        return repository.save(item);
    }
}
