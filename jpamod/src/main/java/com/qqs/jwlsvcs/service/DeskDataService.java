package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.Desk;
import com.qqs.jwlsvcs.repository.DeskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class DeskDataService {
    @Autowired
    private DeskRepository deskRepository;

    @Resource
    private EntityManager entityManager;

    private DataServiceUtils<Desk> citiesUtils = new DataServiceUtils<>();

    public Optional<Desk> findDeskById(Integer id) { return deskRepository.findById(id); }

    public Optional<List<Desk>> SearchDesk(List<SearchCriteria> params) {
        List<Desk> result = citiesUtils.createPredicate(entityManager, params, Desk.class);
        Optional<List<Desk>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }

    public Iterable<Desk> getAllDesk() {
        return deskRepository.findAll();
    }

    @Transactional
    public Desk saveDesk(Desk item) {
        return deskRepository.save(item);
    }
}
