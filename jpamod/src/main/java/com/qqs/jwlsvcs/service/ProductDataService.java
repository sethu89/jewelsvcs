package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.Product;
import com.qqs.jwlsvcs.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class ProductDataService {
    @Autowired
    private ProductRepository productRepository;


    @Resource
    private EntityManager entityManager;

    private DataServiceUtils<Product> jobTransUtil = new DataServiceUtils<>();

    public Optional<Product> getProduct(Integer id) {
        return productRepository.findById(id);
    }

    public Optional<Product> getProductyBarCode(String barCode) {
        return productRepository.findByBarCode(barCode);
    }

    public Optional<List<Product>> searchProduct(List<SearchCriteria> params) {
        List<Product> result = jobTransUtil.createPredicate(entityManager, params, Product.class);
        Optional<List<Product>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }

    public Optional<String> searchBarcodeByProductMasterId(Integer id) {
        return productRepository.searchBarcodeByProductMasterId(id);
    }

    public Optional<List<Product>> searchProductsByProductMasterId(Integer id) {
        return productRepository.searchProductsByProductMasterId(id);
    }




    @Transactional
    public Product saveProduct(Product item) {
        return productRepository.save(item);
    }

}
