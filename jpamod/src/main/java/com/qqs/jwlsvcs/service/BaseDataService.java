package com.qqs.jwlsvcs.service;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public abstract class BaseDataService<T> {

    protected abstract CrudRepository<T, Integer> getRepo();

    protected abstract EntityManager getEntityManager();

    protected abstract Class<T> getModelClass();

    protected abstract DataServiceUtils<T> getSearchUtils();


    public Optional<T> getEntityById(Integer id) {
        return getRepo().findById(id);
    }

    public Iterable<T> getAllEntityById(Iterable<Integer> id) {
        return getRepo().findAllById(id);
    }

    public Optional<List<T>> searchEntities(List<SearchCriteria> params) {
        List<T> result = getSearchUtils().createPredicate(getEntityManager(), params, getModelClass());
        Optional<List<T>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }

    public Iterable<T> getAllEntities(){
        return getRepo().findAll();
    }

    @Transactional
    public T saveEntities(T item) {
        return getRepo().save(item);
    }
    @Transactional
    public Iterable<T> saveAllEntities(Iterable<T> item) {
        return getRepo().saveAll(item);
    }
}
