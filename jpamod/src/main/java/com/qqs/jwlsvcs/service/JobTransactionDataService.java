package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.JobTransaction;
import com.qqs.jwlsvcs.model.JobTransactionData;
import com.qqs.jwlsvcs.repository.JobTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class JobTransactionDataService {
    @Autowired
    private JobTransactionRepository jobTransactionRepository;


    @Resource
    private EntityManager entityManager;

    private DataServiceUtils<JobTransaction> jobTransUtil = new DataServiceUtils<>();

    public Optional<JobTransaction> getJobTransaction(Integer id) {
        return jobTransactionRepository.findById(id);
    }

    public List<JobTransaction> getJobTransactionByProductIds(List<Integer> productIds) {
        return jobTransactionRepository.findAllByProductIdIn(productIds);
    }

    public List<JobTransaction> getJobTransactionByProductId(Integer productId) {
        return jobTransactionRepository.findAllByProductId(productId);
    }
    public Optional<List<JobTransaction>> getByProductId(Integer productId) {
        return jobTransactionRepository.findAllByProductIdOrderByInTimeDesc(productId);
    }

    public Optional<List<JobTransaction>> searchJobTransaction(List<SearchCriteria> params) {
        List<JobTransaction> result = jobTransUtil.createPredicate(entityManager, params, JobTransaction.class);
        Optional<List<JobTransaction>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }




    @Transactional
    public JobTransaction saveJobTransaction(JobTransaction item) {
        return jobTransactionRepository.save(item);
    }

}
