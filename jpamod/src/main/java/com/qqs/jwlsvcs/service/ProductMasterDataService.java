package com.qqs.jwlsvcs.service;

import com.qqs.jwlsvcs.model.ProductMaster;
import com.qqs.jwlsvcs.model.PropAttributes;
import com.qqs.jwlsvcs.repository.ProductMasterRepository;
import com.qqs.jwlsvcs.repository.PropAttributesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class ProductMasterDataService{
    @Autowired
    private ProductMasterRepository productMasterRepository;

    @Autowired
    private PropAttributesRepository propAttributesRepository;

    @Resource
    private EntityManager entityManager;

    private DataServiceUtils<ProductMaster> productMasterUtils = new DataServiceUtils<>();

    public Optional<ProductMaster> getProductMasterById(Integer id) {
        return productMasterRepository.findById(id);
    }

    public Optional<ProductMaster> getProductMasterByBarCodeChar(String barCodeChar) {
        return productMasterRepository.findByBarCodeChar(barCodeChar);
    }

    public Optional<List<ProductMaster>> searchProductMaster(List<SearchCriteria> params) {
        List<ProductMaster> result = productMasterUtils.createPredicate(entityManager, params, ProductMaster.class);
        Optional<List<ProductMaster>> resultSet = Optional.ofNullable(result);
        return resultSet;
    }

    public Iterable<ProductMaster> getAllProductMaster() {
        return productMasterRepository.findAll();
    }

    public Optional<List<PropAttributes>> getPropAttributesByProductMasterId(Integer productMasterId) {
        return propAttributesRepository.getPropAttributesByProductMasterId(productMasterId);
    }

    @Transactional
    public ProductMaster saveProductMaster(ProductMaster item) {
        return productMasterRepository.save(item);
    }

    @Transactional
    public Iterable<PropAttributes> savePropAttributes(Iterable<PropAttributes> item) {
        return propAttributesRepository.saveAll(item);
    }


}
