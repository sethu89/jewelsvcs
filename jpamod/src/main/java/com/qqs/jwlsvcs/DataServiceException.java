package com.qqs.jwlsvcs;

public class DataServiceException extends Exception {
    public DataServiceException(String msg) {
        super(msg);
    }
}
