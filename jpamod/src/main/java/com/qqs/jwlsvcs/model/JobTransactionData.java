package com.qqs.jwlsvcs.model;

public interface JobTransactionData {
    Integer getJobTransactionId();
    String getUser();
    String getProduct();
    String getDesk();
    String getInTime();
    String getOutTime();
    String getJobType();
    String getStatus();
    String getBarCode();
    String getRemarks();

}
