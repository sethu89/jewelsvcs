package com.qqs.jwlsvcs.model;

public interface ProductDetailReportData {
    Integer getId();
    String getProductName();
    String getBarCode();
    String getCurrStatus();
    String getStatusDate();
    String getCustomerName();
    String getRemarks();

}
