package com.qqs.jwlsvcs.model;
import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "codeassociation", schema = "jwlmgmnt", catalog = "")
public class CodeAssociation {
    private int id;
    private String category;
    private String picklist;
    private Timestamp createdDt;
    private Timestamp modifiedDt;
    private Integer createdBy;
    private Integer modifiedBy;


    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Basic
    @Column(name = "picklist")
    public String getPicklist() {
        return picklist;
    }

    public void setPicklist(String picklist) {
        this.picklist = picklist;
    }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;

        CodeAssociation that = (CodeAssociation) object;

        if (id != that.id) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (picklist != null ? !picklist.equals(that.picklist) : that.picklist != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDt != null ? !createdDt.equals(that.createdDt) : that.createdDt != null) return false;
        if (modifiedBy != null ? !modifiedBy.equals(that.modifiedBy) : that.modifiedBy != null) return false;
        if (modifiedDt != null ? !modifiedDt.equals(that.modifiedDt) : that.modifiedDt != null) return false;

        return true;
    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + id;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (picklist != null ? picklist.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDt != null ? createdDt.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDt != null ? modifiedDt.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CodeAssociation{");
        sb.append("id=").append(id);
        sb.append(", category=").append(category);
        sb.append(", picklist='").append(picklist).append('\'');
        sb.append(", createdDt=").append(createdDt);
        sb.append(", modifiedDt=").append(modifiedDt);
        sb.append(", createdBy=").append(createdBy);
        sb.append(", modifiedBy=").append(modifiedBy);
        sb.append('}');
        return sb.toString();
    }
}
