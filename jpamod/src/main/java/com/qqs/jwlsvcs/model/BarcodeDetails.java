package com.qqs.jwlsvcs.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "barcodedetails", schema = "jwlmgmnt", catalog = "")
public class BarcodeDetails {

    private int id;
    private Integer productMasterId;
    private Date generatedDate;
    private String startCode;
    private String endCode;
    private Integer fromNumber;
    private Integer noOfLables;
    private Integer createdBy;
    private Timestamp createdDt;
    private Integer modifiedBy;
    private Timestamp modifiedDt;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "productMasterId")
    public Integer getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(Integer productMasterId) {
        this.productMasterId = productMasterId;
    }

    @Column(name = "generatedDate")
    public Date getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate;
    }

    @Column(name = "fromNumber")
    public Integer getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(Integer fromNumber) {
        this.fromNumber = fromNumber;
    }

    @Column(name = "noOfLables")
    public Integer getNoOfLables() {
        return noOfLables;
    }

    public void setNoOfLables(Integer noOfLables) {
        this.noOfLables = noOfLables;
    }

    @Column(name = "startCode")
    public String getStartCode() {
        return startCode;
    }

    public void setStartCode(String startCode) {
        this.startCode = startCode;
    }

    @Column(name = "endCode")
    public String getEndCode() {
        return endCode;
    }

    public void setEndCode(String endCode) {
        this.endCode = endCode;
    }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BarcodeDetails)) return false;
        BarcodeDetails that = (BarcodeDetails) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getProductMasterId(), that.getProductMasterId()) &&
                Objects.equals(getGeneratedDate(), that.getGeneratedDate()) &&
                Objects.equals(getStartCode(), that.getStartCode()) &&
                Objects.equals(getEndCode(), that.getEndCode()) &&
                Objects.equals(getCreatedBy(), that.getCreatedBy()) &&
                Objects.equals(getCreatedDt(), that.getCreatedDt()) &&
                Objects.equals(getModifiedBy(), that.getModifiedBy()) &&
                Objects.equals(getModifiedDt(), that.getModifiedDt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductMasterId(), getGeneratedDate(), getStartCode(), getEndCode(), getCreatedBy(), getCreatedDt(), getModifiedBy(), getModifiedDt());
    }
}
