package com.qqs.jwlsvcs.model;

public interface JobTransactionReportData {
    String getUser();
    String getBarCode();
    String getOverallStatus();
    String getProduct();
    String getDesk();
    String getInTime();
    String getOutTime();
    String getJobType();
    String getRemarks();

}
