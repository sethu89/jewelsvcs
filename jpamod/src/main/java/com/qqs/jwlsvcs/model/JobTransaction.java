package com.qqs.jwlsvcs.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "jobtransaction", schema = "jwlmgmnt", catalog = "")
public class JobTransaction {
    private int id;
    private int userId;
    private int productId;
    private int deskId;
    private Timestamp inTime;
    private Timestamp outTime;
    private String jobType;
    private String status;
    private String remarks;
    private Timestamp createdDt;
    private Integer createdBy;
    private Integer modifiedBy;
    private Timestamp modifiedDt;
    private Integer stoneCnt;
    private Double prodWeight;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "userId")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "productId")
    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Column(name = "deskId")
    public Integer getDeskId() {
        return deskId;
    }

    public void setDeskId(Integer deskId) { this.deskId = deskId; }

    @Column(name = "inTIme")
    public Timestamp getInTime() { return inTime; }

    public void setInTime(Timestamp inTime) {
        this.inTime = inTime;
    }

    @Column(name = "outTIme")
    public Timestamp getOutTime() { return outTime; }

    public void setOutTime(Timestamp outTime) {
        this.outTime = outTime;
    }

    @Column(name = "jobType")
    public String getJobType() { return jobType; }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "remarks")
    public String getRemarks() { return remarks; }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "stoneCnt")
    public Integer getStoneCnt() { return stoneCnt; }

    public void setStoneCnt(Integer stoneCnt) { this.stoneCnt = stoneCnt; }

    @Column(name = "prodWeight")
    public Double getProdWeight() { return prodWeight; }

    public void setProdWeight(Double prodWeight) { this.prodWeight = prodWeight; }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobTransaction)) return false;
        JobTransaction that = (JobTransaction) o;
        return id == that.id &&
                userId == that.userId &&
                productId == that.productId &&
                deskId == that.deskId &&
                Objects.equals(inTime, that.inTime) &&
                Objects.equals(outTime, that.outTime) &&
                Objects.equals(jobType, that.jobType) &&
                Objects.equals(status, that.status) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(createdDt, that.createdDt) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(modifiedDt, that.modifiedDt) &&
                Objects.equals(stoneCnt, that.stoneCnt) &&
                Objects.equals(prodWeight, that.prodWeight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, productId, deskId, inTime, outTime, jobType, status, remarks, createdDt, createdBy, modifiedBy, modifiedDt, stoneCnt, prodWeight);
    }

    @Override
    public String toString() {
        return "JobTransaction{" +
                "id=" + id +
                ", userId=" + userId +
                ", productId=" + productId +
                ", deskId=" + deskId +
                ", inTime=" + inTime +
                ", outTime=" + outTime +
                ", jobType='" + jobType + '\'' +
                ", status='" + status + '\'' +
                ", remarks='" + remarks + '\'' +
                ", createdDt=" + createdDt +
                ", createdBy=" + createdBy +
                ", modifiedBy=" + modifiedBy +
                ", modifiedDt=" + modifiedDt +
                ", stoneCnt=" + stoneCnt +
                ", prodWeight=" + prodWeight +
                '}';
    }
}
