package com.qqs.jwlsvcs.model;

public interface ProductMasterReportData {
    String getProductName();
    String getBarCodeChar();
    String getLastBarCode();
    Integer getBarCodeCnt();
    Integer getInactiveCnt();
    Integer getSoldCnt();
    Integer getBrokenCnt();
    Integer getStockCnt();

}
