package com.qqs.jwlsvcs.model;


import javax.persistence.*;

@Entity
@Table(name = "states", schema = "jwlmgmnt", catalog = "")
public class States {
    private int id;
    private String name;
    private int country_id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "country_id")
    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }
}
