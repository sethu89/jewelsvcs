package com.qqs.jwlsvcs.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "product", schema = "jwlmgmnt", catalog = "")
public class Product {
    private int id;
    private int productMasterId;
    private String barCode;
    private String processStatus;
    private String productStatus;
    private String finalStatus;
    private Integer finalStatusUserId;
    private Timestamp finalStatusDt;
    private String customerName;
    private String remarks;
    private Timestamp createdDt;
    private Integer createdBy;
    private Integer modifiedBy;
    private Timestamp modifiedDt;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "productMasterId")
    public int getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(int productMasterId) {
        this.productMasterId = productMasterId;
    }

    @Column(name = "barCode")
    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Column(name = "processStatus")
    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    @Column(name = "productStatus")
    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }


    @Column(name = "finalStatus")
    public String getFinalStatus() {
        return finalStatus;
    }
    public void setFinalStatus(String finalStatus) {
        this.finalStatus = finalStatus;
    }

    @Column(name = "finalStatusUserId")
    public Integer getFinalStatusUserId() {
        return finalStatusUserId;
    }

    public void setFinalStatusUserId(Integer finalStatusUserId) {
        this.finalStatusUserId = finalStatusUserId;
    }

    @Column(name = "finalStatusDt")
    public Timestamp getFinalStatusDt() {
        return finalStatusDt;
    }

    public void setFinalStatusDt(Timestamp finalStatusDt) {
        this.finalStatusDt = finalStatusDt;
    }
    @Column(name = "customerName")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    @Column(name = "remarks")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productMasterId=" + productMasterId +
                ", barCode='" + barCode + '\'' +
                ", processStatus='" + processStatus + '\'' +
                ", productStatus='" + productStatus + '\'' +
                ", finalStatus='" + finalStatus + '\'' +
                ", finalStatusUserId=" + finalStatusUserId +
                ", finalStatusDt=" + finalStatusDt +
                ", customerName='" + customerName + '\'' +
                ", remarks='" + remarks + '\'' +
                ", createdDt=" + createdDt +
                ", createdBy=" + createdBy +
                ", modifiedBy=" + modifiedBy +
                ", modifiedDt=" + modifiedDt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getId() == product.getId() &&
                getProductMasterId() == product.getProductMasterId() &&
                Objects.equals(getBarCode(), product.getBarCode()) &&
                Objects.equals(getProcessStatus(), product.getProcessStatus()) &&
                Objects.equals(getProductStatus(), product.getProductStatus()) &&
                Objects.equals(getFinalStatus(), product.getFinalStatus()) &&
                Objects.equals(getFinalStatusUserId(), product.getFinalStatusUserId()) &&
                Objects.equals(getFinalStatusDt(), product.getFinalStatusDt()) &&
                Objects.equals(getCustomerName(), product.getCustomerName()) &&
                Objects.equals(getRemarks(), product.getRemarks()) &&
                Objects.equals(getCreatedDt(), product.getCreatedDt()) &&
                Objects.equals(getCreatedBy(), product.getCreatedBy()) &&
                Objects.equals(getModifiedBy(), product.getModifiedBy()) &&
                Objects.equals(getModifiedDt(), product.getModifiedDt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductMasterId(), getBarCode(), getProcessStatus(), getProductStatus(), getFinalStatus(), getFinalStatusUserId(), getFinalStatusDt(), getCustomerName(), getRemarks(), getCreatedDt(), getCreatedBy(), getModifiedBy(), getModifiedDt());
    }
}
