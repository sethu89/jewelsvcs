package com.qqs.jwlsvcs.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "propattributes", schema = "jwlmgmnt", catalog = "")
public class PropAttributes {
    private int id;
    private Integer productMasterId;
    private String label;
    private String value;
    private String unit;
    private String remarks;
    private Timestamp createdDt;
    private Integer createdBy;
    private Integer modifiedBy;
    private Timestamp modifiedDt;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "productMasterId")
    public Integer getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(Integer productMasterId) {
        this.productMasterId = productMasterId;
    }

    @Column(name = "label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Column(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "unit")

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Column(name = "remarks")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PropAttributes propAttributes = (PropAttributes) o;

        if (id != propAttributes.id) return false;
        if (productMasterId != null ? !productMasterId.equals(propAttributes.productMasterId) : propAttributes.productMasterId != null) return false;
        if (label != null ? !label.equals(propAttributes.label) : propAttributes.label != null) return false;
        if (value != null ? !value.equals(propAttributes.value) : propAttributes.value != null) return false;
        if (unit != null ? !unit.equals(propAttributes.unit) : propAttributes.unit != null) return false;
        if (remarks != null ? !remarks.equals(propAttributes.remarks) : propAttributes.remarks != null) return false;
        if (createdDt != null ? !createdDt.equals(propAttributes.createdDt) : propAttributes.createdDt != null) return false;
        if (createdBy != null ? !createdBy.equals(propAttributes.createdBy) : propAttributes.createdBy != null) return false;
        if (modifiedBy != null ? !modifiedBy.equals(propAttributes.modifiedBy) : propAttributes.modifiedBy != null) return false;
        if (modifiedDt != null ? !modifiedDt.equals(propAttributes.modifiedDt) : propAttributes.modifiedDt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (productMasterId != null ? productMasterId.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (createdDt != null ? createdDt.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDt != null ? modifiedDt.hashCode() : 0);
        return result;
    }


}
