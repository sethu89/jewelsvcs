package com.qqs.jwlsvcs.model;


public interface User {
    int getId();
    String getFirstName();
    String getLastName();
    String getUserName();
}
