package com.qqs.jwlsvcs.model;

public interface DeskReportData {
    String getDeskName();
    String getProductName();
    Integer getInCnt();
    Integer getOutCnt();
    Integer getStoneCnt();
    Double getProdWeight();

}
