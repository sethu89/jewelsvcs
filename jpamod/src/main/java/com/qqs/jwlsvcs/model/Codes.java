package com.qqs.jwlsvcs.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "codes", schema = "jwlmgmnt", catalog = "")
public class Codes {
    private int id;
    private String category;
    private String code;
    private String description;
    private int displaySeq;
//    private String isUserGenerated;
    private Timestamp activeDt;
    private Timestamp expireDt;
    private String notes;
    private Integer createdBy;
    private Timestamp createdDt;
    private Integer modifiedBy;
    private Timestamp modifiedDt;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "displaySeq")
    public int getDisplaySeq() {
        return displaySeq;
    }

    public void setDisplaySeq(int displaySeq) {
        this.displaySeq = displaySeq;
    }

//    @Column(name = "isUserGenerated")
//    public String getIsUserGenerated() {
//        return isUserGenerated;
//    }
//
//    public void setIsUserGenerated(String isUserGenerated) {
//        this.isUserGenerated = isUserGenerated;
//    }

    @Column(name = "activeDt")
    public Timestamp getActiveDt() {
        return activeDt;
    }

    public void setActiveDt(Timestamp activeDt) {
        this.activeDt = activeDt;
    }

    @Column(name = "expireDt")
    public Timestamp getExpireDt() {
        return expireDt;
    }

    public void setExpireDt(Timestamp expireDt) {
        this.expireDt = expireDt;
    }

    @Column(name = "notes")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Codes codes = (Codes) o;

        if (id != codes.id) return false;
        if (category != null ? !category.equals(codes.category) : codes.category != null) return false;
        if (code != null ? !code.equals(codes.code) : codes.code != null) return false;
        if (description != null ? !description.equals(codes.description) : codes.description != null) return false;
        if (activeDt != null ? !activeDt.equals(codes.activeDt) : codes.activeDt != null) return false;
        if (expireDt != null ? !expireDt.equals(codes.expireDt) : codes.expireDt != null) return false;
        if (displaySeq != codes.displaySeq) return false;
        if (notes != null ? !notes.equals(codes.notes) : codes.notes != null) return false;
        if (createdBy != null ? !createdBy.equals(codes.createdBy) : codes.createdBy != null) return false;
        if (createdDt != null ? !createdDt.equals(codes.createdDt) : codes.createdDt != null) return false;
        if (modifiedBy != null ? !modifiedBy.equals(codes.modifiedBy) : codes.modifiedBy != null) return false;
        if (modifiedDt != null ? !modifiedDt.equals(codes.modifiedDt) : codes.modifiedDt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (activeDt != null ? activeDt.hashCode() : 0);
        result = 31 * result + (expireDt != null ? expireDt.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDt != null ? createdDt.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDt != null ? modifiedDt.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Codes{");
        sb.append("id=").append(id);
        sb.append(", category='").append(category).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", desc='").append(description).append('\'');
        sb.append(", activeDt=").append(activeDt);
        sb.append(", expireDt=").append(expireDt);
        sb.append(", notes='").append(notes).append('\'');
        sb.append(", createdBy=").append(createdBy);
        sb.append(", createdDt=").append(createdDt);
        sb.append(", modifiedBy=").append(modifiedBy);
        sb.append(", modifiedDt=").append(modifiedDt);
        sb.append('}');
        return sb.toString();
    }
}
