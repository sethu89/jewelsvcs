package com.qqs.jwlsvcs.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "productmaster", schema = "jwlmgmnt", catalog = "")
public class ProductMaster {
    private int id;
    private String name;
    private String imagePath;
    private String barCodeChar;
    private String status;
    private Timestamp createdDt;
    private Integer createdBy;
    private Integer modifiedBy;
    private Timestamp modifiedDt;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "imagePath")
    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Column(name = "barCodeChar")
    public String getBarCodeChar() {
        return barCodeChar;
    }

    public void setBarCodeChar(String barCodeChar) {
        this.barCodeChar = barCodeChar;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "createdDt", updatable = false)
    public Timestamp getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Timestamp createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "createdBy", updatable = false)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedBy")
    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "modifiedDt")
    public Timestamp getModifiedDt() {
        return modifiedDt;
    }

    public void setModifiedDt(Timestamp modifiedDt) {
        this.modifiedDt = modifiedDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductMaster ProductMaster = (ProductMaster) o;

        if (id != ProductMaster.id) return false;

        if (name != null ? !name.equals(ProductMaster.name) : ProductMaster.name != null) return false;
        if (imagePath != null ? !imagePath.equals(ProductMaster.imagePath) : ProductMaster.imagePath != null) return false;
        if (barCodeChar != null ? !barCodeChar.equals(ProductMaster.barCodeChar) : ProductMaster.barCodeChar != null) return false;
        if (status != null ? !status.equals(ProductMaster.status) : ProductMaster.status != null) return false;
        if (createdDt != null ? !createdDt.equals(ProductMaster.createdDt) : ProductMaster.createdDt != null) return false;
        if (createdBy != null ? !createdBy.equals(ProductMaster.createdBy) : ProductMaster.createdBy != null) return false;
        if (modifiedBy != null ? !modifiedBy.equals(ProductMaster.modifiedBy) : ProductMaster.modifiedBy != null) return false;
        if (modifiedDt != null ? !modifiedDt.equals(ProductMaster.modifiedDt) : ProductMaster.modifiedDt != null) return false;


        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (imagePath != null ? imagePath.hashCode() : 0);
        result = 31 * result + (barCodeChar != null ? barCodeChar.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (createdDt != null ? createdDt.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDt != null ? modifiedDt.hashCode() : 0);

        return result;
    }

}
