package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.PropAttributes;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface PropAttributesRepository extends CrudRepository<PropAttributes, Integer> {

    @Query(value = "select * from propattributes where productMasterId = ?1", nativeQuery = true)
    Optional<List<PropAttributes>> getPropAttributesByProductMasterId(Integer productMasterId);
}
