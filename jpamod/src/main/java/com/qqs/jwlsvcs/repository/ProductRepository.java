package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    Optional<Product> findByBarCode(String barCode);

    @Query(value = "select barCode FROM product WHERE productMasterId = ?1  ORDER BY barCode desc limit 1;", nativeQuery = true)
    Optional<String> searchBarcodeByProductMasterId(Integer productMasterId);

    @Query(value = "select * FROM product WHERE productMasterId = ?1 and processStatus = 'END' ;", nativeQuery = true)
    Optional<List<Product>> searchProductsByProductMasterId(Integer productMasterId);
}
