package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.CodeAssociation;
import org.springframework.data.repository.CrudRepository;

public interface CodeAssociationRepository extends CrudRepository<CodeAssociation, Integer> {

}
