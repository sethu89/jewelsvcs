package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.BarcodeDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BarcodeDetailsRepository extends CrudRepository<BarcodeDetails, Integer> {

//    Optional<List<BarcodeDetails>> findAllByProductMasterId(Integer productMasterId);

    @Query(value = "SELECT * FROM jwlmgmnt.barcodedetails where productMasterId=?1 order by id desc;", nativeQuery = true)
    Optional<List<BarcodeDetails>>  findAllByProductMasterId(Integer productMasterId);


    @Query(value = "SELECT endCode FROM jwlmgmnt.barcodedetails where productMasterId=?1 order by endCode desc limit 1;", nativeQuery = true)
    Optional<String> searchLastBarcodeByProductMasterId(Integer productMasterId);
}

