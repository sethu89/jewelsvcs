package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.ProductMaster;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductMasterRepository extends CrudRepository<ProductMaster, Integer> {
    Optional<ProductMaster> findByBarCodeChar(String barCodeChar);
}

