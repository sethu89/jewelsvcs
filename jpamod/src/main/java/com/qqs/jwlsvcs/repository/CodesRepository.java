package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.Codes;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CodesRepository extends CrudRepository<Codes, Integer> {

    @Query(value = "SELECT * FROM codes WHERE category=?1 AND activeDt < CURDATE() AND (expireDt IS NULL OR expireDt > CURDATE())", nativeQuery = true)
    Optional<List<Codes>> findAllByCategory(String category);

    @Query(value = "SELECT * FROM codes WHERE code=?1 AND activeDt < CURDATE() AND (expireDt IS NULL OR expireDt > CURDATE())", nativeQuery = true)
    Optional<List<Codes>> findAllByCode(String code);
}
