package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JobTransactionReportRepository extends CrudRepository<JobTransaction, Integer> {

    @Query(value="SELECT " +
            " pm.name as product, pro.barCode as barCode, jt.status as overallStatus, usr.firstName as user,  " +
            " dsk.name as desk, jt.inTime as inTime, jt.outTime as outTime, jt.jobType as jobType, jt.remarks as remarks " +
            " FROM `jwlmgmnt`.`jobtransaction` jt  " +
            " INNER JOIN `jwlmgmnt`.`user` usr ON jt.userId = usr.id  " +
            " LEFT OUTER JOIN `jwlmgmnt`.`desk` dsk ON jt.deskId = dsk.id  " +
            " INNER JOIN `jwlmgmnt`.`product` pro on jt.productId = pro.id  " +
            " INNER JOIN `jwlmgmnt`.`productmaster` pm on pro.productMasterId = pm.id  " +
            " WHERE  pm.id = if(?3 = 0, pm.id , ?3) AND " +
            " jt.jobType = if(?4 = 'ALL', jt.jobType , ?4) AND " +
            " date(jt.createdDt) between ?1 AND ?2 " +
            " ORDER BY  pm.name, pro.barCode, jt.inTime desc;", nativeQuery = true)
    List<JobTransactionReportData> getJobTransactionReportData(String fromDate, String toDate,
                                                               Integer productMasterId, String inventoryStatus);

    @Query(value="SELECT " +
            " pm.name as productName, pm.barCodeChar as barCodeChar, MAX(barcode) as lastBarCode,  " +
            " count(barcode) as barCodeCnt, sum(if(processStatus = 'Inactive',1,0)) as inactiveCnt, " +
            " sum(if( finalStatus = 'SLD' ,1,0)) as soldCnt, sum(if(finalStatus = 'BRK',1,0)) as brokenCnt, " +
            " sum(if(finalStatus = 'STK',1,0)) as stockCnt " +
            " from product pd INNER JOIN productmaster pm on pm.id = pd.productMasterId  " +
            " WHERE  pm.id = if(?3 = 0, pm.id , ?3) AND " +
            " date(pd.createdDt) between ?1 AND ?2 " +
            " group by productMasterId order by pm.name;", nativeQuery = true)
    List<ProductMasterReportData> getProductMasterReportData(String fromDate, String toDate, Integer productMasterId);

    @Query(value="SELECT " +
            " pd.id as id, pm.name as productName, pd.barCode as barCode, if(!isnull(pd.finalStatus), cd.description,pd.processStatus) as currStatus,   " +
            " if(!isnull(pd.finalStatus),DATE_FORMAT(pd.finalStatusDt, '%Y-%m-%d'),DATE_FORMAT(pd.modifiedDt, '%Y-%m-%d')) as statusDate,  " +
            " pd.customerName as customerName, pd.remarks as remarks " +
            " from product pd INNER JOIN productmaster pm on pm.id = pd.productMasterId  " +
            " LEFT OUTER JOIN codes cd on cd.code = pd.finalStatus " +
            " WHERE  pm.id = if(?3 = 0, pm.id , ?3) AND " +
            " date(pd.createdDt) between ?1 AND ?2 " +
            " ORDER BY pm.name, pd.barCode;", nativeQuery = true)
    List<ProductDetailReportData> getProductDetailReportData(String fromDate, String toDate, Integer productMasterId);

    @Query(value="SELECT " +
            " dsk.name as deskName, pm.name as productName, sum(if (jobType = 'IN',1,0)) as inCnt, sum(if (jobType = 'OUT',1,0)) as outCnt, " +
            " sum(jt.stoneCnt) as stoneCnt, sum(prodWeight) as prodWeight " +
            " FROM `jwlmgmnt`.`jobtransaction` jt INNER JOIN `jwlmgmnt`.`desk` dsk ON jt.deskId = dsk.id " +
            " INNER JOIN  `jwlmgmnt`.`product` pro on jt.productId = pro.id " +
            " INNER JOIN `jwlmgmnt`.`productmaster` pm on pro.productMasterId = pm.id " +
            " WHERE  pm.id = if(?3 = 0, pm.id , ?3) " +
            " AND date(jt.inTime) between ?1 AND ?2 " +
            " GROUP BY dsk.name, pm.name ORDER BY  dsk.name, pm.name;", nativeQuery = true)
    List<DeskReportData> getDeskReportData(String fromDate, String toDate, Integer productMasterId);

}
