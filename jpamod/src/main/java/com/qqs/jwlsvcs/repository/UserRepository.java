package com.qqs.jwlsvcs.repository;


import com.qqs.jwlsvcs.model.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    @Query(value = "select * from user where firstName like ?1 AND lastName like ?2 AND active = true", nativeQuery = true)
    Optional<List<UserEntity>> findUserEntitiesByName(String firstName, String LastName);

    @Query(value = "select * from user where userName = ?1 AND active = true", nativeQuery = true)
    Optional<UserEntity> findUserEntitiesByUserName(String userName);

}
