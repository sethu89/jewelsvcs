package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.JobTransaction;
import com.qqs.jwlsvcs.model.JobTransactionData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface JobTransactionRepository extends CrudRepository<JobTransaction, Integer> {
    Optional<List<JobTransaction>> findAllByProductIdOrderByInTimeDesc (Integer productId);

    @Query(value="SELECT " +
            " jt.id as jobTransactionId, pm.name as product, usr.firstName as user, dsk.name as desk, jt.remarks as remarks, " +
            " jt.inTime as inTime, jt.outTime as outTime, jt.jobType as jobType, jt.status as status, pro.barCode as barCode " +
            " FROM `jwlmgmnt`.`jobtransaction` jt  " +
            " INNER JOIN `jwlmgmnt`.`user` usr ON jt.userId = usr.id  " +
            " LEFT OUTER JOIN `jwlmgmnt`.`desk` dsk ON jt.deskId = dsk.id  " +
            " INNER JOIN `jwlmgmnt`.`product` pro on jt.productId = pro.id  " +
            " INNER JOIN `jwlmgmnt`.`productmaster` pm on pro.productMasterId = pm.id  " +
            " WHERE  pm.id = if(?3 = 0, pm.id , ?3) AND " +
            " date(jt.createdDt) between ?1 AND ?2 " +
            " ORDER BY  pm.name, pro.barCode, jt.inTime;", nativeQuery = true)
    List<JobTransactionData> getJobTransactionData(String fromDate, String toDate, Integer productMasterId);

    List<JobTransaction> findAllByProductIdIn (List<Integer> productIds);
    List<JobTransaction> findAllByProductId (Integer productId);

}
