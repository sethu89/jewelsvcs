package com.qqs.jwlsvcs.repository;

import com.qqs.jwlsvcs.model.Desk;
import org.springframework.data.repository.CrudRepository;

public interface DeskRepository  extends CrudRepository<Desk, Integer> {

}
