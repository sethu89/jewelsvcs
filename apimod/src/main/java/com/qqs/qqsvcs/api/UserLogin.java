package com.qqs.qqsvcs.api;

import java.sql.Timestamp;
import java.util.Objects;

public class UserLogin {
    private int id;
    private int userId;
    private Timestamp loginDt;
    private Timestamp logoutDt;
    private Integer machineId;
    private String shift;

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public int getUserId() { return userId; }

    public void setUserId(int userId) { this.userId = userId; }

    public Timestamp getLoginDt() { return loginDt; }

    public void setLoginDt(Timestamp loginDt) { this.loginDt = loginDt; }

    public Timestamp getLogoutDt() { return logoutDt; }

    public void setLogoutDt(Timestamp logoutDt) { this.logoutDt = logoutDt; }

    public Integer getMachineId() { return machineId; }

    public void setMachineId(Integer machineId) { this.machineId = machineId; }

    public String getShift() { return shift; }

    public void setShift(String shift) { this.shift = shift; }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        if (!super.equals(object)) return false;

        UserLogin that = (UserLogin) object;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (!Objects.equals(loginDt, that.loginDt)) return false;
        if (!Objects.equals(logoutDt, that.logoutDt)) return false;
        if (!Objects.equals(machineId, that.machineId)) return false;
        if (!Objects.equals(shift, that.shift)) return false;

        return true;
    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + id;
        result = 31 * result + userId;
        result = 31 * result + (loginDt != null ? loginDt.hashCode() : 0);
        result = 31 * result + (logoutDt != null ? logoutDt.hashCode() : 0);
        result = 31 * result + (machineId != null ? machineId.hashCode() : 0);
        result = 31 * result + (shift != null ? shift.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserLogin{");
        sb.append("id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", loginDt='").append(loginDt).append('\'');
        sb.append(", logoutDt='").append(logoutDt).append('\'');
        sb.append(", machineId='").append(machineId).append('\'');
        sb.append(", shift='").append(shift).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
