package com.qqs.qqsvcs.api;

public class ValidationStatus {
    Boolean validationStatus;
    String errorMsg;

    public ValidationStatus() {

    }

    public ValidationStatus(Boolean validationStatus, String errorMsg) {
        this.validationStatus = validationStatus;
        this.errorMsg = errorMsg;
    }

    public Boolean getValidationStatus() { return validationStatus; }

    public void setValidationStatus(Boolean validationStatus) { this.validationStatus = validationStatus; }

    public String getErrorMsg() { return errorMsg; }

    public void setErrorMsg(String errorMsg) { this.errorMsg = errorMsg; }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ValidationStatus{");
        sb.append("validationStatus='").append(validationStatus).append('\'');
        sb.append(", errorMsg='").append(errorMsg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
