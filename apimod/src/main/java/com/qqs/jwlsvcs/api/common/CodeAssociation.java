package com.qqs.jwlsvcs.api.common;

public class CodeAssociation {
    private int id;
    private String category;
    private String picklist;

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getCategory() { return category; }

    public void setCategory(String category) { this.category = category; }

    public String getPicklist() { return picklist; }

    public void setPicklist(String picklist) { this.picklist = picklist; }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CodeAssoication{");
        sb.append("id=").append(id);
        sb.append(", category=").append(category);
        sb.append(", picklist=").append(picklist);
        return sb.toString();
    }
}
