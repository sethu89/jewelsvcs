package com.qqs.jwlsvcs.api;

import java.util.Objects;

public class ProductMasterReportData {
    String productName;
    String barCodeChar;
    String lastBarCode;
    Integer barCodeCnt;
    Integer inactiveCnt;
    Integer activeCnt;
    Integer soldCnt;
    Integer brokenCnt;
    Integer stockCnt;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBarCodeChar() {
        return barCodeChar;
    }

    public void setBarCodeChar(String barCodeChar) {
        this.barCodeChar = barCodeChar;
    }

    public String getLastBarCode() {
        return lastBarCode;
    }

    public void setLastBarCode(String lastBarCode) {
        this.lastBarCode = lastBarCode;
    }

    public Integer getBarCodeCnt() {
        return barCodeCnt;
    }

    public void setBarCodeCnt(Integer barCodeCnt) {
        this.barCodeCnt = barCodeCnt;
    }

    public Integer getInactiveCnt() {
        return inactiveCnt;
    }

    public void setInactiveCnt(Integer inactiveCnt) {
        this.inactiveCnt = inactiveCnt;
    }

    public Integer getActiveCnt() {
        calcActiveCnt();
        return activeCnt;
    }

    public void setActiveCnt(Integer activeCnt) {
        this.activeCnt = activeCnt;
    }

    public Integer getSoldCnt() {
        return soldCnt;
    }

    public void setSoldCnt(Integer soldCnt) {
        this.soldCnt = soldCnt;
    }

    public Integer getBrokenCnt() {
        return brokenCnt;
    }

    public void setBrokenCnt(Integer brokenCnt) {
        this.brokenCnt = brokenCnt;
    }

    public Integer getStockCnt() {
        return stockCnt;
    }

    public void setStockCnt(Integer stockCnt) {
        this.stockCnt = stockCnt;
    }

    public void calcActiveCnt() {
        if(barCodeCnt != null) {
            activeCnt = barCodeCnt - ((inactiveCnt == null ? 0 : inactiveCnt)
                    + (stockCnt == null ? 0 : stockCnt) + (soldCnt == null ? 0 : soldCnt)
                    + (brokenCnt == null ? 0 : brokenCnt) );
        } else {
            activeCnt = 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductMasterReportData)) return false;
        ProductMasterReportData that = (ProductMasterReportData) o;
        return Objects.equals(productName, that.productName) &&
                Objects.equals(barCodeChar, that.barCodeChar) &&
                Objects.equals(lastBarCode, that.lastBarCode) &&
                Objects.equals(barCodeCnt, that.barCodeCnt) &&
                Objects.equals(inactiveCnt, that.inactiveCnt) &&
                Objects.equals(activeCnt, that.activeCnt) &&
                Objects.equals(soldCnt, that.soldCnt) &&
                Objects.equals(brokenCnt, that.brokenCnt) &&
                Objects.equals(stockCnt, that.stockCnt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productName, barCodeChar, lastBarCode, barCodeCnt, inactiveCnt, activeCnt, soldCnt, brokenCnt, stockCnt);
    }

    @Override
    public String toString() {
        return "ProductMasterReportData{" +
                "productName='" + productName + '\'' +
                ", barCodeChar='" + barCodeChar + '\'' +
                ", lastBarCode='" + lastBarCode + '\'' +
                ", barCodeCnt=" + barCodeCnt +
                ", inactiveCnt=" + inactiveCnt +
                ", activeCnt=" + activeCnt +
                ", soldCnt=" + soldCnt +
                ", brokenCnt=" + brokenCnt +
                ", stockCnt=" + stockCnt +
                '}';
    }
}
