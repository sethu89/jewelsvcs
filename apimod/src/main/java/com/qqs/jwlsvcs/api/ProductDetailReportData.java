package com.qqs.jwlsvcs.api;

import java.util.List;
import java.util.Objects;

public class ProductDetailReportData {
    Integer id;
    String productName;
    String barCode;
    String currStatus;
    String statusDate;
    String customerName;
    String remarks;
    List<JobTransaction> jobTransactionList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getCurrStatus() {
        return currStatus;
    }

    public void setCurrStatus(String currStatus) {
        this.currStatus = currStatus;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<JobTransaction> getJobTransactionList() {
        return jobTransactionList;
    }

    public void setJobTransactionList(List<JobTransaction> jobTransactionList) {
        this.jobTransactionList = jobTransactionList;
    }

    @Override
    public String toString() {
        return "ProductDetailReportData{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", barCode='" + barCode + '\'' +
                ", currStatus='" + currStatus + '\'' +
                ", statusDate='" + statusDate + '\'' +
                ", customerName='" + customerName + '\'' +
                ", remarks='" + remarks + '\'' +
                ", jobTransactionList=" + jobTransactionList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductDetailReportData)) return false;
        ProductDetailReportData that = (ProductDetailReportData) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(barCode, that.barCode) &&
                Objects.equals(currStatus, that.currStatus) &&
                Objects.equals(statusDate, that.statusDate) &&
                Objects.equals(customerName, that.customerName) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(jobTransactionList, that.jobTransactionList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productName, barCode, currStatus, statusDate, customerName, remarks, jobTransactionList);
    }
}
