package com.qqs.jwlsvcs.api.translation;

public enum ParentEntityType {
    COMPANY("C"), PLANT("P"), PEOPLE("H"), PART("R"), VENDOR("V");
    String dbCode;

    ParentEntityType(String dbCode) {
        this.dbCode = dbCode;
    }

    public String getDbCode() {
        return dbCode;
    }

    public static ParentEntityType getEntityType(String code) {
        for (ParentEntityType t : ParentEntityType.values()) {
            if (t.dbCode.equalsIgnoreCase(code))
                return t;
        }
        return null;
    }
}
