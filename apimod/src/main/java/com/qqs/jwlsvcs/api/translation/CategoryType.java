package com.qqs.jwlsvcs.api.translation;

public enum CategoryType {
    BUYER("B"), RESPONSIBLE("R");
    String dbCode;

    CategoryType (String dbCode) {
        this.dbCode = dbCode;
    }

    public String getDbCode() {
        return dbCode;
    }

    public static CategoryType getEntityType(String code) {
        for (CategoryType t : CategoryType.values()) {
            if (t.dbCode.equalsIgnoreCase(code))
                return t;
        }
        return null;
    }
}
