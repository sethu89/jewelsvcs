package com.qqs.jwlsvcs.api;

import java.util.Objects;

public class JobTransactionReportData {
    String product;
    String barCode;
    String overallStatus;
    String user;
    String desk;
    String inTime;
    String outTime;
    String jobType;
    String remarks;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getProduct() {
        return product;
    }


    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getOverallStatus() {
        return overallStatus;
    }

    public void setOverallStatus(String overallStatus) {
        this.overallStatus = overallStatus;
    }


    public void setProduct(String product) {
        this.product = product;
    }

    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobTransactionReportData jobTransactionReportData = (JobTransactionReportData) o;

        if (!Objects.equals(user, jobTransactionReportData.user)) return false;
        if (!Objects.equals(product, jobTransactionReportData.product)) return false;
        if (!Objects.equals(barCode, jobTransactionReportData.barCode)) return false;
        if (!Objects.equals(overallStatus, jobTransactionReportData.overallStatus)) return false;
        if (!Objects.equals(desk, jobTransactionReportData.desk)) return false;
        if (!Objects.equals(inTime, jobTransactionReportData.inTime)) return false;
        if (!Objects.equals(outTime, jobTransactionReportData.outTime)) return false;
        if (!Objects.equals(remarks, jobTransactionReportData.remarks)) return false;
        if (!Objects.equals(jobType, jobTransactionReportData.jobType)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (user != null ? user.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + (barCode != null ? barCode.hashCode() : 0);
        result = 31 * result + (overallStatus != null ? overallStatus.hashCode() : 0);
        result = 31 * result + (desk != null ? desk.hashCode() : 0);
        result = 31 * result + (inTime != null ? inTime.hashCode() : 0);
        result = 31 * result + (outTime != null ? outTime.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (jobType != null ? jobType.hashCode() : 0);


        return result;
    }

    @Override
    public String toString() {
        return "JobTransactionReportData{" +
                "product='" + product + '\'' +
                ", barCode='" + barCode + '\'' +
                ", overallStatus='" + overallStatus + '\'' +
                ", user='" + user + '\'' +
                ", desk='" + desk + '\'' +
                ", inTime='" + inTime + '\'' +
                ", outTime='" + outTime + '\'' +
                ", jobType='" + jobType + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
