package com.qqs.jwlsvcs.api;

import java.util.Objects;

public class Barcode {
    private String seriesNo;
    private Integer fromNumber;
    private Integer toNumber;

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public Integer getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(Integer fromNumber) {
        this.fromNumber = fromNumber;
    }

    public Integer getToNumber() {
        return toNumber;
    }

    public void setToNumber(Integer toNumber) {
        this.toNumber = toNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Barcode)) return false;
        Barcode barcode = (Barcode) o;
        return Objects.equals(getSeriesNo(), barcode.getSeriesNo()) &&
                Objects.equals(getFromNumber(), barcode.getFromNumber()) &&
                Objects.equals(getToNumber(), barcode.getToNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSeriesNo(), getFromNumber(), getToNumber());
    }
}
