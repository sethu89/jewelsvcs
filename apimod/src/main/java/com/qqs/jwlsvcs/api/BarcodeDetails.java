package com.qqs.jwlsvcs.api;


import java.util.Date;
import java.util.Objects;

public class BarcodeDetails {
    private int id;
    private Integer productMasterId;
    private String seriesNo;
    private Date generatedDate;
    private Integer fromNumber;
    private Integer noOfLables;
    private String startCode;
    private String endCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(Integer productMasterId) {
        this.productMasterId = productMasterId;
    }

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public Date getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate;
    }

    public Integer getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(Integer fromNumber) {
        this.fromNumber = fromNumber;
    }

    public Integer getNoOfLables() {
        return noOfLables;
    }

    public void setNoOfLables(Integer noOfLables) {
        this.noOfLables = noOfLables;
    }

    public String getStartCode() {
        return startCode;
    }

    public void setStartCode(String startCode) {
        this.startCode = startCode;
    }

    public String getEndCode() {
        return endCode;
    }

    public void setEndCode(String endCode) {
        this.endCode = endCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BarcodeDetails)) return false;
        BarcodeDetails that = (BarcodeDetails) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getProductMasterId(), that.getProductMasterId()) &&
                Objects.equals(getSeriesNo(), that.getSeriesNo()) &&
                Objects.equals(getGeneratedDate(), that.getGeneratedDate()) &&
                Objects.equals(getFromNumber(), that.getFromNumber()) &&
                Objects.equals(getNoOfLables(), that.getNoOfLables()) &&
                Objects.equals(getStartCode(), that.getStartCode()) &&
                Objects.equals(getEndCode(), that.getEndCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductMasterId(), getSeriesNo(), getGeneratedDate(), getFromNumber(), getNoOfLables(), getStartCode(), getEndCode());
    }
}
