package com.qqs.jwlsvcs.api;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class Product {
    private int id;
    private Integer productMasterId;
    private ProductMaster productMaster;
    private String barCode;
    private String processStatus;
    private String productStatus;
    private List<JobTransaction> jobTransactionList;
    private String finalStatus;
    private Integer finalStatusUserId;
    private Timestamp finalStatusDt;
    private String customerName;
    private String remarks;
    private Boolean toDisplayWeight;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(Integer productMasterId) {
        this.productMasterId = productMasterId;
    }

    public ProductMaster getProductMaster() {
        return productMaster;
    }

    public void setProductMaster(ProductMaster productMaster) {
        this.productMaster = productMaster;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public List<JobTransaction> getJobTransactionList() {
        return jobTransactionList;
    }

    public void setJobTransactionList(List<JobTransaction> jobTransactionList) {
        this.jobTransactionList = jobTransactionList;
    }

    public String getFinalStatus() {
        return finalStatus;
    }

    public void setFinalStatus(String finalStatus) {
        this.finalStatus = finalStatus;
    }

    public Integer getFinalStatusUserId() {
        return finalStatusUserId;
    }

    public void setFinalStatusUserId(Integer finalStatusUserId) {
        this.finalStatusUserId = finalStatusUserId;
    }

    public Timestamp getFinalStatusDt() {
        return finalStatusDt;
    }

    public void setFinalStatusDt(Timestamp finalStatusDt) {
        this.finalStatusDt = finalStatusDt;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean getToDisplayWeight() {
        return toDisplayWeight;
    }

    public void setToDisplayWeight(Boolean toDisplayWeight) {
        this.toDisplayWeight = toDisplayWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getId() == product.getId() &&
                Objects.equals(getProductMasterId(), product.getProductMasterId()) &&
                Objects.equals(getProductMaster(), product.getProductMaster()) &&
                Objects.equals(getBarCode(), product.getBarCode()) &&
                Objects.equals(getProcessStatus(), product.getProcessStatus()) &&
                Objects.equals(getProductStatus(), product.getProductStatus()) &&
                Objects.equals(getJobTransactionList(), product.getJobTransactionList()) &&
                Objects.equals(getFinalStatus(), product.getFinalStatus()) &&
                Objects.equals(getFinalStatusUserId(), product.getFinalStatusUserId()) &&
                Objects.equals(getFinalStatusDt(), product.getFinalStatusDt()) &&
                Objects.equals(getCustomerName(), product.getCustomerName()) &&
                Objects.equals(getToDisplayWeight(), product.getToDisplayWeight()) &&
                Objects.equals(getRemarks(), product.getRemarks());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductMasterId(), getProductMaster(), getBarCode(), getProcessStatus(),
                getProductStatus(), getJobTransactionList(), getFinalStatus(), getFinalStatusUserId(),
                getFinalStatusDt(), getCustomerName(), getRemarks(), getToDisplayWeight());
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productMasterId=" + productMasterId +
                ", productMaster=" + productMaster +
                ", barCode='" + barCode + '\'' +
                ", processStatus='" + processStatus + '\'' +
                ", productStatus='" + productStatus + '\'' +
                ", jobTransactionList=" + jobTransactionList +
                ", finalStatus='" + finalStatus + '\'' +
                ", finalStatusUserId=" + finalStatusUserId +
                ", finalStatusDt=" + finalStatusDt +
                ", customerName='" + customerName + '\'' +
                ", hasWeight='" + toDisplayWeight + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
