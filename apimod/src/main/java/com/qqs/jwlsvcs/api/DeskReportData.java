package com.qqs.jwlsvcs.api;

import java.util.Objects;

public class DeskReportData {
    String deskName;
    String productName;
    Integer inCnt;
    Integer outCnt;
    Integer stoneCnt;
    Double prodWeight;

    public String getDeskName() {
        return deskName;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }

    public Integer getInCnt() {
        return inCnt;
    }

    public void setInCnt(Integer inCnt) {
        this.inCnt = inCnt;
    }

    public Integer getOutCnt() {
        return outCnt;
    }

    public void setOutCnt(Integer outCnt) {
        this.outCnt = outCnt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getStoneCnt() {
        return stoneCnt;
    }

    public void setStoneCnt(Integer stoneCnt) {
        this.stoneCnt = stoneCnt;
    }

    public Double getProdWeight() {
        return prodWeight;
    }

    public void setProdWeight(Double prodWeight) {
        this.prodWeight = prodWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeskReportData)) return false;
        DeskReportData that = (DeskReportData) o;
        return Objects.equals(deskName, that.deskName) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(inCnt, that.inCnt) &&
                Objects.equals(outCnt, that.outCnt) &&
                Objects.equals(stoneCnt, that.stoneCnt) &&
                Objects.equals(prodWeight, that.prodWeight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deskName, productName, inCnt, outCnt, stoneCnt, prodWeight);
    }

    @Override
    public String toString() {
        return "DeskReportData{" +
                "deskName='" + deskName + '\'' +
                ", productName='" + productName + '\'' +
                ", inCnt=" + inCnt +
                ", outCnt=" + outCnt +
                ", stoneCnt=" + stoneCnt +
                ", prodWeight=" + prodWeight +
                '}';
    }
}
