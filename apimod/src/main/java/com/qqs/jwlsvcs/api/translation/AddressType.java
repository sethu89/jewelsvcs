package com.qqs.jwlsvcs.api.translation;

public enum AddressType {
    DELIVERY("D"), BILLING("B"), PHYSICAL("P"), CORPORATE("C");
    String dbCode;

    AddressType(String dbCode) {
        this.dbCode = dbCode;
    }

    public String getDbCode() {
        return dbCode;
    }

    public static AddressType getEntityType(String code) {
        for (AddressType t : AddressType.values()) {
            if (t.dbCode.equalsIgnoreCase(code))
                return t;
        }
        return null;
    }
}
