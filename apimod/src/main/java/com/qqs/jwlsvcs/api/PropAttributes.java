package com.qqs.jwlsvcs.api;

public class PropAttributes {
    private int id;
    private Integer productMasterId;
    private String label;
    private String value;
    private String unit;
    private String remarks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(Integer productMasterId) {
        this.productMasterId = productMasterId;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PropAttributes propAttributes = (PropAttributes) o;

        if (id != propAttributes.id) return false;
        if (productMasterId != null ? !productMasterId.equals(propAttributes.productMasterId) : propAttributes.productMasterId != null) return false;
        if (label != null ? !label.equals(propAttributes.label) : propAttributes.label != null) return false;
        if (value != null ? !value.equals(propAttributes.value) : propAttributes.value != null) return false;
        if (unit != null ? !unit.equals(propAttributes.unit) : propAttributes.unit != null) return false;
        if (remarks != null ? !remarks.equals(propAttributes.remarks) : propAttributes.remarks != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (productMasterId != null ? productMasterId.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        return result;
    }


}
