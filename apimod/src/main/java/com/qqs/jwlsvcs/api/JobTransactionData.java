package com.qqs.jwlsvcs.api;

import java.sql.Timestamp;
import java.util.Objects;

public class JobTransactionData {
    Integer jobTransactionId;
    String product;
    String user;
    String desk;
    String inTime;
    String outTime;
    String jobType;
    String status;
    String barCode;
    String remarks;

    public Integer getJobTransactionId() {
        return jobTransactionId;
    }

    public void setJobTransactionId(Integer jobTransactionId) {
        this.jobTransactionId = jobTransactionId;
    }


    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "JobTransactionData{" +
                ", jobTransactionId=" + jobTransactionId +
                ", product=" + product +
                ", user=" + user +
                ", desk=" + desk +
                ", inTime=" + inTime +
                ", outTime=" + outTime +
                ", status=" + status +
                ", barCode=" + barCode +
                ", remarks=" + remarks +
                ", jobType='" + jobType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobTransactionData)) return false;
        JobTransactionData that = (JobTransactionData) o;
        return Objects.equals(product, that.product) &&
                Objects.equals(jobTransactionId, that.jobTransactionId) &&
                Objects.equals(user, that.user) &&
                Objects.equals(desk, that.desk) &&
                Objects.equals(inTime, that.inTime) &&
                Objects.equals(outTime, that.outTime) &&
                Objects.equals(status, that.status) &&
                Objects.equals(barCode, that.barCode) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(jobType, that.jobType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobTransactionId, product, user, desk, inTime, outTime, jobType, status, barCode, remarks);
    }
}
