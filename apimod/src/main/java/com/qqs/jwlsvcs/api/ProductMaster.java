package com.qqs.jwlsvcs.api;

import java.util.List;

public class ProductMaster {
    private int id;
    private String name;
    private String imagePath;
    private List<PropAttributes> propAttributes;
    private String barCodeChar;
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<PropAttributes> getPropAttributes() {
        return propAttributes;
    }

    public void setPropAttributes(List<PropAttributes> propAttributes) {
        this.propAttributes = propAttributes;
    }

    public String getBarCodeChar() {
        return barCodeChar;
    }

    public void setBarCodeChar(String barCodeChar) {
        this.barCodeChar = barCodeChar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPropValue(String propLabel) {
        String propValue = "";
        if(propAttributes != null) {
            for (PropAttributes propAttr : propAttributes) {
                if (propAttr.getLabel().equalsIgnoreCase(propLabel)) {
                    return propAttr.getValue();
                }
            }
        }
        return propValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductMaster ProductMaster = (ProductMaster) o;

        if (id != ProductMaster.id) return false;

        if (name != null ? !name.equals(ProductMaster.name) : ProductMaster.name != null) return false;
        if (imagePath != null ? !imagePath.equals(ProductMaster.imagePath) : ProductMaster.imagePath != null) return false;
        if (propAttributes != null ? !propAttributes.equals(ProductMaster.propAttributes) : ProductMaster.propAttributes != null) return false;
        if (barCodeChar != null ? !barCodeChar.equals(ProductMaster.barCodeChar) : ProductMaster.barCodeChar != null) return false;
        if (status != null ? !status.equals(ProductMaster.status) : ProductMaster.status != null) return false;


        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (imagePath != null ? imagePath.hashCode() : 0);
        result = 31 * result + (propAttributes != null ? propAttributes.hashCode() : 0);
        result = 31 * result + (barCodeChar != null ? barCodeChar.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);

        return result;
    }
}

