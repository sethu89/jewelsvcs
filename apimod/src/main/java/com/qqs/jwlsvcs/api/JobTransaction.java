package com.qqs.jwlsvcs.api;

import java.sql.Timestamp;
import java.util.Objects;

public class JobTransaction {
    private Integer id;
    private Integer productId;
    private Integer userId;
    private Integer deskId;
    private String deskName;
    private Timestamp inTime;
    private Timestamp outTime;
    private String jobType;
    private String status;
    private String remarks;
    private Integer stoneCnt;
    private Double prodWeight;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDeskId() {
        return deskId;
    }

    public void setDeskId(Integer deskId) {
        this.deskId = deskId;
    }

    public Timestamp getInTime() {
        return inTime;
    }

    public void setInTime(Timestamp inTime) {
        this.inTime = inTime;
    }

    public Timestamp getOutTime() {
        return outTime;
    }

    public void setOutTime(Timestamp outTime) {
        this.outTime = outTime;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDeskName() {
        return deskName;
    }

    public void setDeskName(String deskName) {
        this.deskName = deskName;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStoneCnt() {
        return stoneCnt;
    }

    public void setStoneCnt(Integer stoneCnt) {
        this.stoneCnt = stoneCnt;
    }

    public Double getProdWeight() {
        return prodWeight;
    }

    public void setProdWeight(Double prodWeight) {
        this.prodWeight = prodWeight;
    }

    @Override
    public String toString() {
        return "JobTransaction{" +
                "id=" + id +
                ", productId=" + productId +
                ", userId=" + userId +
                ", deskId=" + deskId +
                ", deskName='" + deskName + '\'' +
                ", inTime=" + inTime +
                ", outTime=" + outTime +
                ", jobType='" + jobType + '\'' +
                ", status='" + status + '\'' +
                ", remarks='" + remarks + '\'' +
                ", stoneCnt=" + stoneCnt +
                ", prodWeight=" + prodWeight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobTransaction)) return false;
        JobTransaction that = (JobTransaction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(deskId, that.deskId) &&
                Objects.equals(deskName, that.deskName) &&
                Objects.equals(inTime, that.inTime) &&
                Objects.equals(outTime, that.outTime) &&
                Objects.equals(jobType, that.jobType) &&
                Objects.equals(status, that.status) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(stoneCnt, that.stoneCnt) &&
                Objects.equals(prodWeight, that.prodWeight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productId, userId, deskId, deskName, inTime, outTime, jobType, status,
                remarks, stoneCnt, prodWeight);
    }
}
