package com.qqs.jwlsvcs.api;

import java.util.Objects;

public class DashboardDetails {
    private Integer poCount;
    private Integer partCount;
    private Integer parentPartCount;


    public Integer getPoCount() { return poCount; }

    public void setPoCount(Integer poCount) { this.poCount = poCount; }

    public Integer getPartCount() { return partCount; }

    public void setPartCount(Integer partCount) { this.partCount = partCount; }

    public Integer getParentPartCount() { return parentPartCount; }

    public void setParentPartCount(Integer parentPartCount) { this.parentPartCount = parentPartCount; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DashboardDetails)) return false;
        DashboardDetails that = (DashboardDetails) o;
        return Objects.equals(poCount, that.poCount) &&
                Objects.equals(partCount, that.partCount) &&
                Objects.equals(parentPartCount, that.parentPartCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(poCount, partCount, parentPartCount);
    }

    @Override
    public String toString() {
        return "DashboardDetails{" +
                "poCount=" + poCount +
                ", partCount=" + partCount +
                ", parentPartCount=" + parentPartCount +
                '}';
    }
}
