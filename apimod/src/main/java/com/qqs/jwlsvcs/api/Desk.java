package com.qqs.jwlsvcs.api;

import java.util.Objects;

public class Desk {
    private int id;
    private String name;
    private String description;
    private String status;
    private String deskType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String accountNumber) {
        this.description = accountNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeskType() {
        return deskType;
    }

    public void setDeskType(String deskType) {
        this.deskType = deskType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Desk)) return false;
        Desk desk = (Desk) o;
        return id == desk.id &&
                Objects.equals(name, desk.name) &&
                Objects.equals(description, desk.description) &&
                Objects.equals(status, desk.status) &&
                Objects.equals(deskType, desk.deskType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, status, deskType);
    }

    @Override
    public String toString() {
        return "Desk{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", deskType='" + deskType + '\'' +
                '}';
    }
}
