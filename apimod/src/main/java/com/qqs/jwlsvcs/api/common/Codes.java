package com.qqs.jwlsvcs.api.common;

import java.sql.Timestamp;

public class Codes {
    private int id;
    private String category;
    private String code;
    private int displaySeq;
    private String isUserGenerated;
    private String description;
    private Timestamp activeDt;
    private Timestamp expireDt;
    private String notes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDisplaySeq() {
        return displaySeq;
    }

    public void setDisplaySeq(int displaySeq) {
        this.displaySeq = displaySeq;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsUserGenerated() {
        return isUserGenerated;
    }

    public void setIsUserGenerated(String isUserGenerated) {
        this.isUserGenerated = isUserGenerated;
    }

    public Timestamp getActiveDt() {
        return activeDt;
    }

    public void setActiveDt(Timestamp activeDt) {
        this.activeDt = activeDt;
    }

    public Timestamp getExpireDt() {
        return expireDt;
    }

    public void setExpireDt(Timestamp expireDt) {
        this.expireDt = expireDt;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Codes{");
        sb.append("id=").append(id);
        sb.append(", category='").append(category).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", isUserGenerated='").append(isUserGenerated).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", activeDt=").append(activeDt);
        sb.append(", expireDt=").append(expireDt);
        sb.append(", notes='").append(notes).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
